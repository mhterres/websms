-- Name: dbversion; Type: TABLE; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE TABLE dbversion (
	    dbversion bigint NOT NULL
);


ALTER TABLE public.dbversion OWNER TO smsuser;

--
-- Data for Name: dbversion; Type: TABLE DATA; Schema: public; Owner: smsuser
--

INSERT INTO dbversion VALUES (1);

--- SMS

ALTER TABLE public.sms ADD COLUMN  codigo character varying(20);


