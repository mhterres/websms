--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

DROP INDEX public.smsrec_idx_number;
DROP INDEX public.smsrec_idx_data;
DROP INDEX public.sms_idx_numero;
DROP INDEX public.sms_idx_dongle;
DROP INDEX public.sms_idx_dataprocessado;
DROP INDEX public.sms_idx_dataenvio;
DROP INDEX public.sms_idx_data;
DROP INDEX public.optout_idx_number;
DROP INDEX public.optout_idx_data;
DROP INDEX public.groupsitems_idx_group_id;
DROP INDEX public.group_idx_name;
DROP INDEX public.dongle_idx_number;
DROP INDEX public.dongle_idx_imei;
DROP INDEX public.dongle_idx_dongle;
DROP INDEX public.controle_idx_dongle_id;
DROP INDEX public.controle_idx_dongle_data;
DROP INDEX public.controle_idx_data;
ALTER TABLE ONLY public.smsrec DROP CONSTRAINT smsrec_pkey;
ALTER TABLE ONLY public.sms DROP CONSTRAINT sms_pkey;
ALTER TABLE ONLY public.optout DROP CONSTRAINT optout_pkey;
ALTER TABLE ONLY public.groupitems DROP CONSTRAINT groupitems_pkey;
ALTER TABLE ONLY public.groups DROP CONSTRAINT group_pkey;
ALTER TABLE ONLY public.dongles DROP CONSTRAINT dongles_pkey;
ALTER TABLE ONLY public.controle DROP CONSTRAINT controle_pkey;
DROP TABLE public.smsrec;
DROP SEQUENCE public.smsrec_id_seq;
DROP TABLE public.sms;
DROP SEQUENCE public.sms_id_seq;
DROP TABLE public.optout;
DROP SEQUENCE public.optout_id_seq;
DROP TABLE public.groups;
DROP TABLE public.groupitems;
DROP SEQUENCE public.groupitem_id_seq;
DROP SEQUENCE public.group_id_seq;
DROP TABLE public.dongles;
DROP SEQUENCE public.dongles_id_seq;
DROP TABLE public.controle;
DROP SEQUENCE public.controle_id_seq;
DROP EXTENSION plpgsql;
DROP SCHEMA public;
--
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

--
-- Name: controle_id_seq; Type: SEQUENCE; Schema: public; Owner: smsuser
--

CREATE SEQUENCE controle_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 1000000
    CACHE 1;


ALTER TABLE public.controle_id_seq OWNER TO smsuser;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: controle; Type: TABLE; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE TABLE controle (
    id bigint DEFAULT nextval('controle_id_seq'::regclass) NOT NULL,
    dongle_id bigint NOT NULL,
    data timestamp without time zone DEFAULT now() NOT NULL,
    smsenviados bigint DEFAULT 0 NOT NULL
);


ALTER TABLE public.controle OWNER TO smsuser;

--
-- Name: dongles_id_seq; Type: SEQUENCE; Schema: public; Owner: smsuser
--

CREATE SEQUENCE dongles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 1000000
    CACHE 1;


ALTER TABLE public.dongles_id_seq OWNER TO smsuser;

--
-- Name: dongles; Type: TABLE; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE TABLE dongles (
    id bigint DEFAULT nextval('dongles_id_seq'::regclass) NOT NULL,
    dongle character varying(10) NOT NULL,
    ativo boolean DEFAULT true NOT NULL,
    number character varying(30) NOT NULL,
    imei character varying(30) NOT NULL,
    provider character varying(30),
    model character varying(30),
    sendsms boolean DEFAULT false NOT NULL,
    tipo character varying(1) DEFAULT 'T'::character varying NOT NULL
);


ALTER TABLE public.dongles OWNER TO smsuser;

--
-- Name: group_id_seq; Type: SEQUENCE; Schema: public; Owner: smsuser
--

CREATE SEQUENCE group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000000
    CACHE 1;


ALTER TABLE public.group_id_seq OWNER TO smsuser;

--
-- Name: groupitem_id_seq; Type: SEQUENCE; Schema: public; Owner: smsuser
--

CREATE SEQUENCE groupitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 10000000
    CACHE 1;


ALTER TABLE public.groupitem_id_seq OWNER TO smsuser;

--
-- Name: groupitems; Type: TABLE; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE TABLE groupitems (
    id bigint DEFAULT nextval('groupitem_id_seq'::regclass) NOT NULL,
    number character varying(20) NOT NULL,
    name character varying(100) NOT NULL,
    group_id bigint NOT NULL
);


ALTER TABLE public.groupitems OWNER TO smsuser;

--
-- Name: groups; Type: TABLE; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE TABLE groups (
    id bigint DEFAULT nextval('group_id_seq'::regclass) NOT NULL,
    created timestamp without time zone DEFAULT now() NOT NULL,
    last_update timestamp without time zone DEFAULT now() NOT NULL,
    name character varying(50) NOT NULL,
    membersnum bigint NOT NULL
);


ALTER TABLE public.groups OWNER TO smsuser;

--
-- Name: optout_id_seq; Type: SEQUENCE; Schema: public; Owner: smsuser
--

CREATE SEQUENCE optout_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 1000000
    CACHE 1;


ALTER TABLE public.optout_id_seq OWNER TO smsuser;

--
-- Name: optout; Type: TABLE; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE TABLE optout (
    id bigint DEFAULT nextval('optout_id_seq'::regclass) NOT NULL,
    number character varying(30) NOT NULL,
    data timestamp without time zone NOT NULL,
    tipo character varying(1) DEFAULT 'A'::character varying NOT NULL
);


ALTER TABLE public.optout OWNER TO smsuser;

--
-- Name: sms_id_seq; Type: SEQUENCE; Schema: public; Owner: smsuser
--

CREATE SEQUENCE sms_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 1000000
    CACHE 1;


ALTER TABLE public.sms_id_seq OWNER TO smsuser;

--
-- Name: sms; Type: TABLE; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE TABLE sms (
    id bigint DEFAULT nextval('sms_id_seq'::regclass) NOT NULL,
    data timestamp without time zone NOT NULL,
    numero character varying(30) NOT NULL,
    mensagem character varying(280) NOT NULL,
    dongle character(10),
    status character(1),
    dataenvio timestamp without time zone,
    tentativas bigint DEFAULT 0 NOT NULL,
    tipoenvio character varying(1) DEFAULT 'a'::character varying NOT NULL,
    agendado_ini timestamp without time zone,
    agendado_fim timestamp without time zone,
    sent_queue_id character varying(50),
    dataprocessado timestamp without time zone
);


ALTER TABLE public.sms OWNER TO smsuser;

--
-- Name: smsrec_id_seq; Type: SEQUENCE; Schema: public; Owner: smsuser
--

CREATE SEQUENCE smsrec_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 1000000
    CACHE 1;


ALTER TABLE public.smsrec_id_seq OWNER TO smsuser;

--
-- Name: smsrec; Type: TABLE; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE TABLE smsrec (
    id bigint DEFAULT nextval('smsrec_id_seq'::regclass) NOT NULL,
    data timestamp without time zone NOT NULL,
    number character varying(30) NOT NULL,
    dongle character(10) NOT NULL,
    dongle_number character varying(30) NOT NULL,
    message character varying(800) NOT NULL
);


ALTER TABLE public.smsrec OWNER TO smsuser;

--
-- Name: controle_pkey; Type: CONSTRAINT; Schema: public; Owner: smsuser; Tablespace: 
--

ALTER TABLE ONLY controle
    ADD CONSTRAINT controle_pkey PRIMARY KEY (id);


--
-- Name: dongles_pkey; Type: CONSTRAINT; Schema: public; Owner: smsuser; Tablespace: 
--

ALTER TABLE ONLY dongles
    ADD CONSTRAINT dongles_pkey PRIMARY KEY (id);


--
-- Name: group_pkey; Type: CONSTRAINT; Schema: public; Owner: smsuser; Tablespace: 
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT group_pkey PRIMARY KEY (id);


--
-- Name: groupitems_pkey; Type: CONSTRAINT; Schema: public; Owner: smsuser; Tablespace: 
--

ALTER TABLE ONLY groupitems
    ADD CONSTRAINT groupitems_pkey PRIMARY KEY (id);


--
-- Name: optout_pkey; Type: CONSTRAINT; Schema: public; Owner: smsuser; Tablespace: 
--

ALTER TABLE ONLY optout
    ADD CONSTRAINT optout_pkey PRIMARY KEY (id);


--
-- Name: sms_pkey; Type: CONSTRAINT; Schema: public; Owner: smsuser; Tablespace: 
--

ALTER TABLE ONLY sms
    ADD CONSTRAINT sms_pkey PRIMARY KEY (id);


--
-- Name: smsrec_pkey; Type: CONSTRAINT; Schema: public; Owner: smsuser; Tablespace: 
--

ALTER TABLE ONLY smsrec
    ADD CONSTRAINT smsrec_pkey PRIMARY KEY (id);


--
-- Name: controle_idx_data; Type: INDEX; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE INDEX controle_idx_data ON controle USING btree (data);


--
-- Name: controle_idx_dongle_data; Type: INDEX; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE INDEX controle_idx_dongle_data ON controle USING btree (dongle_id, data);


--
-- Name: controle_idx_dongle_id; Type: INDEX; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE INDEX controle_idx_dongle_id ON controle USING btree (dongle_id);


--
-- Name: dongle_idx_dongle; Type: INDEX; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE INDEX dongle_idx_dongle ON dongles USING btree (dongle);


--
-- Name: dongle_idx_imei; Type: INDEX; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE INDEX dongle_idx_imei ON dongles USING btree (imei);


--
-- Name: dongle_idx_number; Type: INDEX; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE INDEX dongle_idx_number ON dongles USING btree (number);


--
-- Name: group_idx_name; Type: INDEX; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE INDEX group_idx_name ON groups USING btree (name);


--
-- Name: groupsitems_idx_group_id; Type: INDEX; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE INDEX groupsitems_idx_group_id ON groupitems USING btree (group_id);


--
-- Name: optout_idx_data; Type: INDEX; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE INDEX optout_idx_data ON optout USING btree (data);


--
-- Name: optout_idx_number; Type: INDEX; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE INDEX optout_idx_number ON optout USING btree (number);


--
-- Name: sms_idx_data; Type: INDEX; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE INDEX sms_idx_data ON sms USING btree (data);


--
-- Name: sms_idx_dataenvio; Type: INDEX; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE INDEX sms_idx_dataenvio ON sms USING btree (dataenvio);


--
-- Name: sms_idx_dataprocessado; Type: INDEX; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE INDEX sms_idx_dataprocessado ON sms USING btree (dataprocessado);


--
-- Name: sms_idx_dongle; Type: INDEX; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE INDEX sms_idx_dongle ON sms USING btree (dongle);


--
-- Name: sms_idx_numero; Type: INDEX; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE INDEX sms_idx_numero ON sms USING btree (numero);


--
-- Name: smsrec_idx_data; Type: INDEX; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE INDEX smsrec_idx_data ON smsrec USING btree (data);


--
-- Name: smsrec_idx_number; Type: INDEX; Schema: public; Owner: smsuser; Tablespace: 
--

CREATE INDEX smsrec_idx_number ON smsrec USING btree (number);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

