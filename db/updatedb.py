#!/usr/bin/python

# Update the database from the current version until the latest version found
# in /usr/local/graal/db/diff

import ConfigParser
import datetime
import glob
import psycopg2
import psycopg2.extras
import os
import subprocess
import sys

DIR = '/usr/local/sbin/websms/db/updates'
os.chdir(DIR)

# Read PGSql configuration and connect to there
config = ConfigParser.RawConfigParser()
config.read('/etc/websms/sms.conf')
dsn = 'dbname=%s host=%s user=%s password=%s' % (config.get('database',
'dbname'), config.get('database', 'dbhost'), config.get('database', 'dbuser'),
config.get('database', 'dbpass'))

conn = psycopg2.connect(dsn)
curs = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

# Get current version
sql = 'SELECT dbversion FROM dbversion LIMIT 1'
curs.execute(sql)
current = curs.fetchone()[0]
print 'Current version: %d' % (current)

# Get available versions
allFiles = sorted(glob.glob('[0-9][0-9][0-9][0-9][0-9].sql'))

# Keep only newer updates
updates = {}
for f in allFiles:
  id = int(f[:5])
  if id <= current: continue

  updates[id] = f

if not updates:
  print 'There are no available updates for the database'
  sys.exit()

print 'There are %d available updates for the database' % (len(updates))

for f in sorted(updates.keys()):
  print 'Applying update %s' % (updates[f])
  sql = open(updates[f]).read()

  try:
    curs.execute(sql)

  except Exception, err: # do not continue if errors found
    print >> sys.stderr, '\x1b[1;31mError executing %s\x1b[0m' % (updates[f])
    print >> sys.stderr, '\x1b[1;33m%s\x1b[0m' % str(err)
    print >> sys.stderr, 'Current version now: %d' % (current)

    conn.rollback()
    sys.exit(1)

  else: # no errors in the update, commit the db
    sql = 'UPDATE dbversion SET dbversion=%s'
    curs.execute(sql, (f,))
    conn.commit()
    current = f

print '\x1b[1;32mUpdate %d applied successfully\x1b[0m' % (f)

