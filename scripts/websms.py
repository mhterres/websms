#!/usr/bin/python
# -*- coding: utf-8 -*-

# websms.py
# SMS via Web
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-05-06
#

import web
import sys
import time
import datetime
import unicodedata

sys.path.insert(1,'/usr/lib/python2.7/dist-packages/sms')

from dbpgsql import DBPgsql
from config import Config
from defs import getHeader
from defs import getFooter
from defs import geraGraph
from defs import remover_acentos

cfg=Config()
DB=DBPgsql(cfg)

# DEBUG
log = open('/tmp/websms.log', 'a')
sys.stderr = log

# data atual
dataatual=time.strftime("%Y-%d-%d")


urls = (
  '/', 'index',
  '/webservice/', 'send_webservice',
  '/enviasms/', 'send_sms',
  '/envialote/', 'send_smsbatch',
  '/envialotecustom/', 'send_smsbatch_custom',
  '/envialotecustommsg/', 'send_smsbatch_custommsg',
  '/enviagrupo/', 'send_smsgroup',
  '/enviados/', 'list_sent',
  '/agendados/', 'list_sched',
  '/recebidos/', 'list_received',
  '/optout/', 'list_optout',
  '/newoptout/', 'new_optout',
  '/canais/', 'list_dongles',
  '/controle/', 'list_dongle_control',
  '/smsresumo/', 'sms_day_summary',
  '/graf_stat/', 'graph_stats',
  '/graf_evol/', 'graph_evol',
  '/group_list/', 'group_list',
  '/group_upload/', 'group_upload',
  '/group_members/(.*)', 'group_members',
  '/delete_group/(.*)', 'delete_group',
  '/delete_groupitem/', 'delete_groupitem'
)

app = web.application(urls, globals())

class index:

	def GET(self):

		header=getHeader("WebSMS","Principal")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Principal")
		page=web.template.frender("%s/index.html" % cfg.htmlpath)
		return page(header,footer,data)

class send_smsbatch: 

	def GET(self):

		header=getHeader("WebSMS","Envio de SMS em lote")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Envio de SMS em lote")
		form=web.template.frender("%s/send_sms_batch.html" % cfg.htmlpath)
		return form(header,footer,data)

class send_smsgroup: 

	def GET(self):

		header=getHeader("WebSMS","Envio de SMS para grupo")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Envio de SMS para grupo")

		groups=DB.dropdown_Groups()

		dropdown=createDropdownGroups(groups,"smsgroup")

		form=web.template.frender("%s/send_sms_group.html" % cfg.htmlpath)
		return form(header,footer,dropdown,data)


class send_smsbatch_custom: 

	def GET(self):

		header=getHeader("WebSMS","Envio de SMS em lote personalizado")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Envio de SMS em lote personalizado")
		form=web.template.frender("%s/send_sms_batch_custom.html" % cfg.htmlpath)
		return form(header,footer,data)

class send_smsbatch_custommsg:

	def GET(self):

		header=getHeader("WebSMS","Envio de SMS em lote com mensagens individuais")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Envio de SMS em lote com mensagens individuais")
		form=web.template.frender("%s/send_sms_batch_custommsg.html" % cfg.htmlpath)
		return form(header,footer,data)


class send_sms: 

	def GET(self): 
	
		header=getHeader("WebSMS","Envio de SMS avulso")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Envio de SMS avulso")
		form=web.template.frender("%s/send_one.html" % cfg.htmlpath)
		return form(header,footer,data)

class list_received:

	def GET(self):

		getDongle=web.input(dongle=None)
		getDataIni=web.input(start=None)
		getDataFim=web.input(end=None)
		getTelefone=web.input(telefone=None)

		dongle=getDongle.dongle
		dataini=getDataIni.start
		datafim=getDataFim.end
		telefone=getTelefone.telefone

		if telefone==None:

			telefone=""
	
		values={}
		values['dataini']=dataini
		values['datafim']=datafim
		values['telefone']=telefone

		sms=DB.smsRec(dongle,dataini,datafim,telefone)
		dongles=DB.dropdown_Dongles()

		dropdown=createDropdownDongle(dongle,dongles,"recebidos")

		header=getHeader("WebSMS","Lista de SMS Recebidos")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Lista de SMS Recebidos",sms=sms)
		lista=web.template.frender("%s/smsrec.html"  % cfg.htmlpath)
		return lista(header,footer,values,dropdown,data,dongles)

class list_sent:

	def GET(self):

		getDongle=web.input(dongle=None)
		getDataIni=web.input(start=None)
		getDataFim=web.input(end=None)
		getTelefone=web.input(telefone=None)

		dongle=getDongle.dongle
		dataini=getDataIni.start
		datafim=getDataFim.end
		telefone=getTelefone.telefone

		if telefone==None:

			telefone=""

		values={}
		values['dataini']=dataini
		values['datafim']=datafim
		values['telefone']=telefone

		sms=DB.smsSent(dongle,dataini,datafim,telefone)
		dongles=DB.dropdown_Dongles()

		dropdown=createDropdownDongle(dongle,dongles,"enviados")

		header=getHeader("WebSMS","Lista de SMS Enviados")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Lista de SMS Enviados",sms=sms)
		lista=web.template.frender("%s/smssent.html" % cfg.htmlpath)
		return lista(header,footer,values,dropdown,data,dongles,"1 2 3")

class list_sched:

	def GET(self):

		getDataIni=web.input(start=None)
		getDataFim=web.input(end=None)
		getTelefone=web.input(telefone=None)

		dataini=getDataIni.start
		datafim=getDataFim.end
		telefone=getTelefone.telefone

		if telefone==None:

			telefone=""
	
		values={}
		values['dataini']=dataini
		values['datafim']=datafim
		values['telefone']=telefone

		sms=DB.smsSched(dataini,datafim,telefone)

		header=getHeader("WebSMS","Lista de SMS Agendados")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Lista de SMS Agendados",sms=sms)
		lista=web.template.frender("%s/smssched.html" % cfg.htmlpath)
		return lista(header,footer,values,data,"1 2 3")


class list_optout:

	def GET(self):

		getNumero=web.input(numero=None)
		getDataIni=web.input(start=None)
		getDataFim=web.input(end=None)

		numero=getNumero.numero
		dataini=getDataIni.start
		datafim=getDataFim.end

		values={}
		values['numero']=numero
		values['dataini']=dataini
		values['datafim']=datafim

		optout=DB.optOut(numero,dataini,datafim)

		header=getHeader("WebSMS","Opt-Out")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Opt-out",optouts=optout,mensagem="")
		lista=web.template.frender("%s/optout.html" % cfg.htmlpath)
		return lista(header,footer,values,data)

class new_optout:

	def GET(self):

		getNovoNumero=web.input(novonumero=None)

		novonumero=getNovoNumero.novonumero

		newOptout=DB.isNewOptOut(novonumero)

		if newOptout:

			DB.insertOptOut(datetime.datetime.strftime(datetime.datetime.now(), '%Y-%m-%d %H:%M:%S'),novonumero,"M")
			mensagem="Telefone %s adicionado ao opt-out." % novonumero
		else:

			mensagem="Telefone %s existente no opt-out." % novonumero

		numero=""
		dataini=""
		datafim=""

		values={}
		values['numero']=numero
		values['dataini']=dataini
		values['datafim']=datafim

		optout=DB.optOut(numero,dataini,datafim)

		header=getHeader("WebSMS","Opt-Out")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Opt-out",optouts=optout,mensagem=mensagem)
		lista=web.template.frender("%s/optout.html" % cfg.htmlpath)
		return lista(header,footer,values,data)


class list_dongles:

	def GET(self):

		values=web.input()
		ids=[]

		if len(values.keys()) >0 :

			for item in values.keys():

				id=item.split("_")[0]

				if not id in ids:

					ids.append(id)

					try:

						strAtivo=values['%s_ativo' % id]
					except:

						strAtivo="off"

					try:

						strSendsms=values['%s_sendsms' % id]
					except:

						strSendsms="off"

					DB.updateDongleStatus(id,strAtivo,strSendsms)

		dongle=DB.Dongles()

		header=getHeader("WebSMS","Lista de canais GSM")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Lista de canais GSM",dongles=dongle)
		lista=web.template.frender("%s/dongles.html" % cfg.htmlpath)
		return lista(header,footer,data)

class list_dongle_control:

	def GET(self):

		getDongle=web.input(dongle=None)
		getDataIni=web.input(start=None)
		getDataFim=web.input(end=None)

		dongle=getDongle.dongle
		dataini=getDataIni.start
		datafim=getDataFim.end

		values={}
		values['dataini']=dataini
		values['datafim']=datafim

		dias=DB.Dongle_Control(dongle,dataini,datafim)
		dongles=DB.dropdown_Dongles()

		dropdown=createDropdownDongle(dongle,dongles,"dongle_control")

		header=getHeader("WebSMS","Lista de Envio diário por canal GSM")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Lista de Envio diário por canal GSM",dias=dias)
		lista=web.template.frender("%s/dongle_control.html" % cfg.htmlpath)
		return lista(header,footer,values,dropdown,data,dongles)

class graph_stats:

	def GET(self):

		getDataIni=web.input(start=None)
		getDataFim=web.input(end=None)

		dataini=getDataIni.start
		datafim=getDataFim.end

		if dataini=="":

			dataini=None
			datafim=None

		if (datafim is None or datafim=="") and dataini is not None:

			datafim=dataini

		values={}
		values['dataini']=dataini
		values['datafim']=datafim

		graph=""

		if dataini is not None:

			dados=DB.getSMSStat(dataini,datafim)
			graph=geraGraph("pie",dados,"Estatisticas de SMS do periodo")

		header=getHeader("WebSMS","Gráfico Estatístico de Envio de SMS")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Gráfico Estatístico de Envio de SMS",graph=graph)
		lista=web.template.frender("%s/graph_stat.html" % cfg.htmlpath)
		return lista(header,footer,values,data)

class graph_evol:

	def GET(self):

		getDataIni=web.input(start=None)
		getDataFim=web.input(end=None)

		dataini=getDataIni.start
		datafim=getDataFim.end

		if dataini=="":

			dataini=None
			datafim=None

		if (datafim is None or datafim=="") and dataini is not None:

			datafim=dataini

		values={}
		values['dataini']=dataini
		values['datafim']=datafim

		graph=""

		dias=[]

		if dataini is not None:

			dt=time.mktime(datetime.datetime.strptime("%s 00:00:00" % dataini, "%Y-%m-%d %H:%M:%S").timetuple())
			dtfim=time.mktime(datetime.datetime.strptime("%s 23:59:59" % datafim, "%Y-%m-%d %H:%M:%S").timetuple())

			while dt < dtfim:

				dias.append(DB.resumoDia(datetime.datetime.fromtimestamp(dt).strftime('%Y-%m-%d')))

				dt+=86400

			graph=geraGraph("line",dias,"Grafico de Evolucao de Envio de SMS")

		header=getHeader("WebSMS","Gráfico de Evolução de Envio de SMS")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Gráfico de Evolução de Envio de SMS",graph=graph)
		lista=web.template.frender("%s/graph_evol.html" % cfg.htmlpath)
		return lista(header,footer,values,data)


class sms_day_summary:

	def GET(self):

		getDataIni=web.input(start=None)
		getDataFim=web.input(end=None)

		dataini=getDataIni.start
		datafim=getDataFim.end

		if dataini=="":

			dataini=None
			datafim=None

		if (datafim is None or datafim=="") and dataini is not None:

			datafim=dataini

		values={}
		values['dataini']=dataini
		values['datafim']=datafim

		dias=[]

		if dataini is not None:

			dt=time.mktime(datetime.datetime.strptime("%s 00:00:00" % dataini, "%Y-%m-%d %H:%M:%S").timetuple())
			dtfim=time.mktime(datetime.datetime.strptime("%s 23:59:59" % datafim, "%Y-%m-%d %H:%M:%S").timetuple())

			while dt < dtfim:

				dias.append(DB.resumoDia(datetime.datetime.fromtimestamp(dt).strftime('%Y-%m-%d')))

				dt+=86400

		header=getHeader("WebSMS","Lista de Envio diário de SMS")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Lista de Envio diário de SMS",dias=dias)
		lista=web.template.frender("%s/smsresumo.html" % cfg.htmlpath)
		return lista(header,footer,values,data)

class send_webservice:

	def GET(self):

		getNumero=web.input(numero="")
		getMensagem=web.input(mensagem="")
		getDataInicial=web.input(datainicial="")
		getDataFinal=web.input(datafinal="")
		
		numero=getNumero.numero
		mensagem=getMensagem.mensagem
		dataini=getDataInicial.datainicial
		datafim=getDataFinal.datafinal

		output ='<?xml version="1.0" encoding="UTF-8"?>\n'
		output+='<sms>\n'
	
		if not VerificaNumero(numero):

			output+="  <erro>Número inválido.</erro>"
		else:
	
			if mensagem=="":

				output+="  <erro>Mensagem não informada.</erro>"
			else:

				erro=False

				if datafim!="" and dataini=="":

					erro=True
					output+="  <erro>Data Inicial não informada.</erro>"

				if dataini!="":

					if not erro:

						try:
							dt=datetime.datetime.strptime(dataini, "%Y-%m-%d %H:%M:%S")
						except:

							erro=True
							output+="  <erro>Data inicial inválida.</erro>"
						else:
							dtini=time.mktime(dt.timetuple())

				if datafim!="":

					if not erro:

						try:
							dt=datetime.datetime.strptime(datafim, "%Y-%m-%d %H:%M:%S")
						except:

							erro=True
							output+="  <erro>Data final inválida.</erro>"
						else:
							dtfim=time.mktime(dt.timetuple())

					if not erro:

						if dtfim<dtini:
							erro=True
							output+="  <erro>Data final menor que data inicial.</erro>"
	
				if not erro:

						try:

							mensagem_processada=remover_acentos(mensagem)
						except:

							try:

								mensagem_processada=unicodedata.normalize('NFKD', mensagem).encode('ascii', 'ignore')

							except:

								mensagem_processada=mensagem

						if len(mensagem_processada)>160:
						
							erro=True
							output+="  <erro>Mensagem maior que 160 caracteres.</erro>"

						if not erro:
	
							DB.insertSentSMS(time.strftime("%Y-%m-%d %H:%M:%S"),numero,remover_acentos(mensagem_processada),"W",dataini,datafim)

							output+="  <numero>%s</numero>\n" % numero
							output+="  <mensagem>%s</mensagem>\n" % remover_acentos(mensagem_processada)

							if dataini !="":

								output+="  <data_inicial>%s</data_inicial>\n" % dataini

							if datafim !="":
								output+="  <data_final>%s</data_final>\n" % datafim
								output+="  <status>Agendado</status>\n" 

		output+="</sms>"

		return output

class group_list:

	def GET(self):

		groups=DB.groups()

		header=getHeader("WebSMS","Lista de Grupos")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Lista de Grupos",grupos=groups)
		lista=web.template.frender("%s/groups_list.html"  % cfg.htmlpath)
		return lista(header,footer,data)

class group_members:

	def GET(self,group_id):

		dados=DB.membersGroup(group_id)

		header=getHeader("WebSMS","Lista de membros do grupo %s" % dados[0])
		footer=getFooter()
		group_id_hidden='<input type="hidden" id="group_id" name="group_id" value="%s">' % group_id
		data=dict(title="WebSMS",subtitle="Lista de membros do grupo %s" % dados[0],membros=dados[1],group_id_hidden=group_id_hidden)
		lista=web.template.frender("%s/group_members.html"  % cfg.htmlpath)
		return lista(header,footer,data)


class group_upload: 

	def GET(self):

		header=getHeader("WebSMS","Criação/Edição de grupo de envio")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Criação/Edição de grupo de envio")
		form=web.template.frender("%s/group_upload.html" % cfg.htmlpath)
		return form(header,footer,data)

class delete_group: 
 
	def GET(self,group_id): 
 
		DB.deleteGroup(group_id) 

		header=getHeader("WebSMS","Deleção de grupo de envio")
		footer=getFooter()
		data=dict(title="WebSMS",subtitle="Deleção de grupo de envio")
		form=web.template.frender("%s/group_delete.html" % cfg.htmlpath)
		return form(header,footer,data)

class delete_groupitem: 
 
	def GET(self): 

		getId=web.input(id=0)
		getGroup_id=web.input(group_id=0)

		id=getId.id
		group_id=getGroup_id.group_id

		members_num=DB.getGroupMembersNum(group_id)

		DB.deleteGroupItem(id) 
		DB.updateGroupMembersNumber(group_id,members_num-1)

		header=getHeader("WebSMS","Deleção de membro de grupo de envio")
		footer=getFooter()
		button='<input type="button" onClick="window.location.href = \'/group_members/%s\';" class="button tiny" value="Volta para lista de membros">' % group_id
		
		data=dict(title="WebSMS",subtitle="Deleção de membro de grupo de envio",button=button)
		form=web.template.frender("%s/group_deleteitem.html" % cfg.htmlpath)
		return form(header,footer,data)



def VerificaNumero(numero):

	if len(numero)<8:

		return False

	try:

		valor = int(numero)

	except ValueError:

		return False

	return True

def createDropdownDongle(selected_dongle,dongles,form):

	dd=''
	dd+='<select name="dongle" form="%s">' % form
	dd+='	<option value=""></option>'

	for dongle in dongles:

		selected=""
		if dongle==selected_dongle:

			selected="selected"

		dd+='<option %s value=%s>%s</option>' % (selected,dongle,dongle)

	dd+='</select>'
	return dd

def createDropdownGroups(groups,form):

	dd=''
	dd+='<select name="grupo">' 
	dd+='	<option value=""></option>'

	for group in groups:

		dd+='<option value="%s">%s</option>' % (group,group)

	dd+='</select>'
	return dd


if __name__  == "__main__":
	app.run()

