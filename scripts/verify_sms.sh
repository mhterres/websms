#! /bin/bash
/usr/bin/pgrep websms.py

if [ "$?" == "1" ]; then

   echo "Iniciando websms..."
   /etc/init.d/websms start
fi

/usr/bin/pgrep smsstatus.py

if [ "$?" == "1" ]; then

   echo "Iniciando smsstatus..."
   /etc/init.d/smsstatus start
fi

