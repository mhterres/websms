#!/usr/bin/python
# -*- coding: utf-8 -*-

# Envia SMS via CLI
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-05-11
#

import sys
from unidecode import unidecode

sys.path.insert(1,'/usr/lib/python2.7/dist-packages/sms')

from dbpgsql import DBPgsql
from config import Config
from dongle import Dongle

cfg=Config()
DB=DBPgsql(cfg)
dongleSMS=Dongle()

# DEBUG
log = open('/tmp/sendsms_cli.log', 'a')
sys.stderr = log

if len(sys.argv) > 1:

	numero = sys.argv[1]

	if len(sys.argv) > 2:

		mensagem = unidecode(sys.argv[2].decode('utf-8'))

		if len(sys.argv) > 3:

			dongle=sys.argv[3]
		else:
			dongle=""

	try:

		valor = int(numero)
	except ValueError:
		print "O parâmetro deve ser um número."
		print "Ex: ./sendsms_cli.py <numero> <mensagem> [dongle]"
		sys.exit(0)
else:

	print "Informe o número de destino e a mensagem."
	print "Ex: ./sendsms_cli.py <numero> <mensagem> [dongle]"
	sys.exit(0)
validDongle=False

if dongle=="":

	retorno=DB.getDongleToSMS()

	if len(retorno)>0:

		id=retorno['id']
		dongle=retorno['dongle']

		validDongle=True
	else:

		errMotivo="Todos os dongles com envio esgotado para esta data."
else:

	retorno=DB.infoDongle(dongle)

	if retorno['valid']:

		validDongle=True

		id=retorno['id']
	else:

		errMotivo=retorno['errMotivo']
		
if validDongle:		

	if not DB.optoutNumber(numero):

		print "Enviando SMS para %s através do Dongle %s id %i. Mensagem: %s" % (numero,dongle,id,mensagem)

		dongleSMS.ami.sendSMS(dongle,numero,mensagem)
	else:

		print "Destino %s inválido - Opt-Out" % numero

else:

	print "Dongle %s inválido. - Motivo %s" % (dongle,errMotivo)
