#!/usr/bin/python
# -*- coding: utf-8 -*-

# Atualiza os dongles do sistema no DB
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-03-18
#

import sys

sys.path.insert(1,'/usr/lib/python2.7/dist-packages/sms')

from dongle import Dongle
from config import Config

cfg=Config()

d=Dongle()
d.donglesUpdate()

dongles=d.getDongles()

for device in dongles:

	if cfg.debug=="1":

		print "Processing dongle %s." % device

	data=d.getDongleInfo([device])
	d.updateDB(data)

d.verifyDB()
