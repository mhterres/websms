#!/usr/bin/python
# -*- coding: utf-8 -*-

# dictio.py
#
# Class to create and manipulate AMI dictionaries
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-03-18
#

class ManagerDict:

	def __init__(self,text):

		# split the text
		lines = text.split('\n')

		d={}

		for item in lines:

			data=item.split(":",1)

			try:

				title=data[0]
			except:

				title=""

			try:

				value=data[1]
			except:

				value=""


			d.update({title:value})

		self.dicti=d

	def getitem(self,index):

		i=self.dicti[index]
		i=i.replace(" ","\ ")
		return i

class ManagerDictEvents:

	def __init__(self,text,myevent):

		self.description = "Generate dictionary of a specific event."

		# split the text
		lines = text.split('\n')

		i=0

		dicio={}
		events=[]

		fulldicio={}
		dicioitem=0

		if text.find('Event: ' + myevent) == -1:

			self.isvalid=False
		else:

			self.isvalid=True

			self.events=[]
			self.items=0
			self.fulldicti={}
			self.dictiitems=0

			for myitem in lines:

				data=myitem.split(":",1)

				try:

					title=data[0]
				except:

					title=""

				try:

					value=data[1]
				except:

					value=""

				if (title == "Event" and value.find(myevent) > 0 and len(dicio) == 0):

					i+=1
					events.append({})
					dicioitem+=1
					fulldicio.update({title:value})

					dicio.update({title:value})
					events[i-1].update({title:value})
			
				elif (title == "Event" and len(dicio)>0):

					dicioitem+=1
					fulldicio.update({title:value})

					if len(dicio) > 0:

						events[i-1].update({title:value})

						dicio.clear()
						dicio={}

					if value.find(myevent) > 0:

						i+=1
						events.append({})
						dicioitem+=1
						fulldicio.update({title:value})
						
						events[i-1].update({title:value})
						dicio.update({title:value})

				elif (title != "" and len(dicio)>0):

					dicioitem+=1
					fulldicio.update({title:value})

					events[i-1].update({title:value})
					dicio.update({title:value})

			self.items=i
			self.events=events
			self.dicti=fulldicio
			self.dictiitems=dicioitem
			self.text=text

class ManagerDictEventsType:

  def __init__(self,text,type,myevent):

    self.description = "Gera lista de dicionários dos itens de um evento específico."

    # split the text
    linhas = text.split('\n')

    i=0

    dicio={}
    events=[]

    fulldicio={}
    dicioitem=0

    if text.find(type + ': ' + myevent) == -1:

      self.isvalid=False
    else:

      self.isvalid=True

      self.events=[]
      self.items=0
      self.fulldicti={}
      self.dictiitems=0

      for myitem in linhas:

        dado=myitem.split(":",1)

        try:

          title=dado[0]
        except:

          title=""

        try:
          value=dado[1]
        except:

          value=""

        if (title == type and value.find(myevent) > 0 and len(dicio) == 0):

          i+=1
          events.append({})
          dicioitem+=1
          fulldicio.update({title:value})

          dicio.update({title:value})
          events[i-1].update({title:value})

        elif (title == type and len(dicio)>0):

          dicioitem+=1
          fulldicio.update({title:value})

          if len(dicio) > 0:

            events[i-1].update({title:value})

            dicio.clear()
            dicio={}

          if value.find(myevent) > 0:

            i+=1
            events.append({})
            dicioitem+=1
            fulldicio.update({title:value})

            events[i-1].update({title:value})
            dicio.update({title:value})

        elif (title != "" and len(dicio)>0):

          dicioitem+=1
          fulldicio.update({title:value})

          events[i-1].update({title:value})
          dicio.update({title:value})

      self.items=i
      self.events=events
      self.dicti=fulldicio
      self.dictiitems=dicioitem
      self.text=text


