#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
		defs.py
		Classe com defs genéricas
		2015/05/18
		Marcelo Hartmann Terres <mhterres@gmail.com>
"""

import os
import sys
import time
import pygal
import datetime

from pygal.style import LightStyle
from pygal.style import LightSolarizedStyle

from unicodedata import normalize

sys.path.insert(1,'/usr/lib/python2.7/dist-packages/sms')

from config import Config


def getHeader(title,subtitle):

	cfg=Config()
	file=open("%s/header.html" % cfg.htmlpath)
	text=file.read().replace("%TITLE%",title).replace("%SUBTITLE%",subtitle)

	return text

def getFooter():

	cfg=Config()
	file=open("%s/footer.html" % cfg.htmlpath)
	text=file.read()

	return text

def remover_acentos(txt, codif='utf-8'):

	t=open("/tmp/x123","w")

	t.write("%s\n" % txt)

	replaces=[]
	replaces.append(["ã","a"])
	replaces.append(["à","a"])
	replaces.append(["á","a"])
	replaces.append(["â","a"])
	replaces.append(["é","e"])
	replaces.append(["í","i"])
	replaces.append(["ó","o"])
	replaces.append(["ô","o"])
	replaces.append(["ú","u"])
	replaces.append(["ü","u"])
	replaces.append(["ç","c"])

	replaces.append(["Ã","A"])
	replaces.append(["Á","A"])
	replaces.append(["À","A"])
	replaces.append(["Â","A"])
	replaces.append(["É","E"])
	replaces.append(["Í","I"])
	replaces.append(["Ó","O"])
	replaces.append(["Ô","O"])
	replaces.append(["Ú","U"])
	replaces.append(["Ü","U"])
	replaces.append(["Ç","C"])

	for replace in replaces:

		txt=txt.replace(replace[0],replace[1])

	t.write("%s\n" % txt)

	try:

		retorno=normalize('NFKD', txt.decode(codif)).encode('ASCII','ignore')
	except:

		try:

			retorno=normalize('NFKD', txt).encode('ASCII','ignore')
		except:
	
			retorno=txt

	t.write("%s\n" % retorno)
	t.close()

	return retorno

def validTime(cfg,data):

	dt=datetime.datetime.strptime(data, "%Y-%m-%d %H:%M:%S")
	weekday=dt.weekday()

	if weekday>4:

		wk=True
		hr=cfg.horario_envio_weekend
	else:

		wk=False
		hr=cfg.horario_envio_workdays

	agora=time.strftime("%Y-%m-%d %H:%M:%S")

	hrini="%s %s:00" % (time.strftime("%Y-%m-%d"),hr.split("-")[0])
	hrfim="%s %s:00" % (time.strftime("%Y-%m-%d"),hr.split("-")[1])

	#print hrini
	#print hrfim
	
	if int(cfg.sendweekdays[weekday])!=1:

		print "Dia da semana não permitido para envio de SMS."
		retorno=False
	else:

		retorno=True

	if retorno:

		if agora >= hrini and agora <= hrfim:

			retorno=True
		else:

			print "Horário não permitido para envio de SMS."
			retorno=False

	return retorno

def geraGraph(tipo,dados,titulo):

	output=""
	if tipo=="pie":

		pie_chart = pygal.Pie(disable_xml_declaration=True,style=LightSolarizedStyle,width=400,height=400,explicit_size=True)
		pie_chart.title = titulo

		for dado in dados[1]:

			pie_chart.add(dado['label'],dado['perc'])

		chart=pie_chart.render()

		output+="<table border=0 style='border: 0px;'>"
		output+=" <tr>"
		output+="  <td>"

		output+="   <table border=0 style='border: 0px;'>"

		output+="    <tr>"
		output+="     <td><b>Total</b></td>"
		output+="     <td style='text-align: right;'>%s</td>" % int(dados[0])
		output+="     <td>enviados</td>" 
		output+="    </tr>"

		for dado in dados[1]:

			output+="    <tr>"
			output+="     <td><b>%s</b></td>" % dado['label']
			output+="     <td style='text-align: right;'>%s</td>" % dado['total']
			output+="     <td style='text-align: right;'>%s %%</td>" % dado['perc']
			output+="    </tr>"

		output+="   </table>"

		output+="  </td>"

		output+="  <td>"

		output+=chart
		output+="  </td>"
		output+=" </tr>"
		output+="</table>"

	elif tipo=="line":

		line_chart = pygal.Line(disable_xml_declaration=True,style=LightSolarizedStyle,width=800,height=400,explicit_size=True,fill=True)
		line_chart.title = titulo

		Datas=[]
		Total=[]
		Aguardando=[]
		NaFila=[]
		Enviados=[]
		Cancelados=[]
		Vencidos=[]
		Optout=[]

		for dia in dados:

			Datas.append("%s/%s" % (dia['data'][8:10],dia['data'][5:7]))
			Total.append(int(dia['total']))
			Aguardando.append(int(dia['aguardando']))
			NaFila.append(int(dia['nafila']))
			Enviados.append(int(dia['enviado']))
			Cancelados.append(int(dia['cancelado']))
			Vencidos.append(int(dia['vencido']))
			Optout.append(int(dia['optout']))

		line_chart.x_labels=Datas

		line_chart.add('Total',Total)
		line_chart.add('Aguardando',Aguardando)
		line_chart.add('Na Fila',NaFila)
		line_chart.add('Enviados',Enviados)
		line_chart.add('Cancelados',Cancelados)
		line_chart.add('Vencidos',Vencidos)
		line_chart.add('Optout',Optout)

		chart=line_chart.render()

		output=chart

	return output
