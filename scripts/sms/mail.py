#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    mail.py
    Classe para envio de e-mails
    2015/01/26
    Marcelo Hartmann Terres <mhterres@gmail.com>
"""

import os
import sys
import smtplib

from email.mime.text import MIMEText

class Mail:

  def __init__(self):

    self.description = 'E-mails'

  def send_msg(self,cfg,email_dest,subject,body):

    msg = MIMEText(body)

    msg['Subject'] = subject
    msg['From'] = cfg.email_send
    msg['To'] = email_dest

    s = smtplib.SMTP(cfg.email_host)

    s.sendmail(cfg.email_send, [email_dest], msg.as_string())

    s.quit()

    return True

  def send_msg_alert(self,cfg,subject,body):

    msg = MIMEText(body)

    msg['Subject'] = subject
    msg['From'] = cfg.email_alerta_send
    msg['To'] = cfg.email_alerta_dest

    s = smtplib.SMTP(cfg.email_host)

    s.sendmail(cfg.email_alerta_send, [cfg.email_alerta_dest], msg.as_string())

    s.quit()

    return True

