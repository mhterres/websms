import time

def recv_basic(the_socket):
	total_data=[]
	while True:
		data = the_socket.recv(8192)
		print "data %s" %data
		if not data: break
		total_data.append(data)
	return ''.join(total_data)

def recv_timeout(the_socket,timeout=2):
	the_socket.setblocking(0)
	total_data=[];data='';begin=time.time()
	while 1:
		#if you got some data, then break after wait sec
		if total_data and time.time()-begin>timeout:
			break
		#if you got no data at all, wait a little longer
		elif time.time()-begin>timeout*2:
			break
		try:
			data=the_socket.recv(8192)
			if data:
				total_data.append(data)
				begin=time.time()
			else:
				time.sleep(0.1)
		except:
			pass
	return ''.join(total_data)	
