#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
		dbpgsql.py
		Classe para acesso ao DB Postgres
		2015/01/27
		Marcelo Hartmann Terres <mhterres@gmail.com>
"""
import time
import logging
import psycopg2
import psycopg2.extras

from ami	import AMI
from dictio import ManagerDict
from dictio import ManagerDictEventsType


class DBPgsql:

	def __init__(self,config):

		self.description = 'Postgresql Database'

		self.dsn = 'dbname=%s host=%s user=%s password=%s' % (config.db_name,config.db_host,config.db_user,config.db_pwd)

		self.conn = psycopg2.connect(self.dsn)
		self.cfg = config

	def insertRecSMS(self,data,dname,dnum,number,msg):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sql = "INSERT INTO smsrec(data,dongle,dongle_number,number,message) VALUES ('%s','%s','%s','%s','%s');" % (data,dname,dnum,number,msg)

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:
			self.conn.commit()

		curs.close()

	def insertSentSMS(self,data,numero,msg,tipo,dataini,datafim):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		msg=msg.replace("'","")

		if datafim != "":
			
			sql = "INSERT INTO sms(data,numero,mensagem,tipoenvio,status,agendado_ini,agendado_fim) VALUES ('%s','%s','%s','%s','A','%s','%s');" % (data,numero,msg,tipo,dataini,datafim)
		else:

			if dataini != "":
			
				sql = "INSERT INTO sms(data,numero,mensagem,tipoenvio,status,agendado_ini) VALUES ('%s','%s','%s','%s','A','%s');" % (data,numero,msg,tipo,dataini)

			else:

				sql = "INSERT INTO sms(data,numero,mensagem,tipoenvio,status) VALUES ('%s','%s','%s','%s','A');" % (data,numero,msg,tipo)
		#print sql

		retorno=True
		try:
			curs.execute(sql)

		except:
			self.conn.rollback()
			retorno=False

		else:
			self.conn.commit()

		curs.close()
		return retorno
	
	def insertOptOut(self,data,number,tipo):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sql = "INSERT INTO optout(data,number,tipo) VALUES ('%s','%s','%s');" % (data,number,tipo)

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:
			self.conn.commit()

		curs.close()

	def smsRec(self,dongle,dataini,datafim,telefone):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sms=[]

		sql = "SELECT * FROM smsrec "

		where=""

		if dataini is not None:

			if dongle!= "":

				where="WHERE dongle='%s'" % dongle

			if dataini != "" or datafim != "":

				if where == "":

					where = "WHERE "
				else:

					where += " AND "

				if dataini != "":

					where += "data >= '%s 00:00:00'" % dataini

				if datafim != "":

					if dataini != "":

						where+=" AND "

					where+="data <= '%s 23:59:59'" % datafim

		if telefone != "":

			if where == "":

				where = "WHERE "
			else:

				where += " AND "
			where += "numero LIKE '%" + telefone.strip() + "%'"
		
		sql += "%s ORDER BY data;" % where

#		if where != "":
		#print sql

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row = curs.fetchone()

				while row is not None:

					sms.append(dict(id=row[0],data=row[1],number=row[2],dongle=row[3],dongle_number=row[4],message=row[5]))
					row = curs.fetchone()

			self.conn.commit()

		curs.close()
		
		return sms

	def smsSent(self,dongle,dataini,datafim,telefone):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sms=[]

		sql = "SELECT * FROM sms "

		where=""

		if dataini is not None:
		
			if dongle!= "":

				where="WHERE dongle='%s'" % dongle

			if dataini != "" or datafim != "":

				if where == "":

					where = "WHERE "
				else:

					where += " AND "

				if dataini != "":

					where += "data >= '%s 00:00:00'" % dataini

				if datafim != "":

					if dataini != "":

						where+=" AND "

					where+="data <= '%s 23:59:59'" % datafim

		if telefone != "":

			if where == "":

				where = "WHERE "
			else:

				where += " AND "
			where += "numero LIKE '%" + telefone.strip() + "%'"
		
		sql += "%s ORDER BY data;" % where

		if where != "":

			try:
				curs.execute(sql)

			except:
				self.conn.rollback()
	
			else:

				if curs.rowcount>0:

					row = curs.fetchone()

					while row is not None:

						sms.append(dict(id=row[0],data=row[1],numero=row[2],mensagem=row[3],dongle=row[4],status=row[5],dataenvio=row[6],tentativas=row[7],tipoenvio=row[8],agendado_ini=row[9],agendado_fim=row[10]))
						row = curs.fetchone()

				self.conn.commit()

			curs.close()

		return sms

	def smsSched(self,dataini,datafim,telefone):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sms=[]

		sql = "SELECT * FROM sms "

		where=""

		if dataini is not None:

			if dataini != "" and datafim == "":

				where = "WHERE agendado_ini >= '%s 00:00:00'" % dataini
		
			elif dataini != "" and  datafim != "":

				where = "WHERE (agendado_ini >= '%s 00:00:00' AND agendado_ini <= '%s 23:59:59')" % (dataini,datafim)
				#where = "WHERE (agendado_ini >= '%s 00:00:00' AND agendado_fim <= '%s 23:59:59')" % (dataini,datafim)
				#where+= " OR (agendado_ini >= '%s 00:00:00' AND agendado_ini <= '%s 23:59:59' AND agendado_fim IS NULL)" % (dataini,datafim)

		if telefone != "":

			if where == "":

				where = "WHERE "
			else:

				where += " AND "
			where += "numero LIKE '%" + telefone.strip() + "%'"
	
		sql += "%s ORDER BY agendado_ini;" % where

		if where != "":

			try:
				curs.execute(sql)

			except:
				self.conn.rollback()

			else:

				if curs.rowcount>0:

					row = curs.fetchone()

					while row is not None:

						sms.append(dict(id=row[0],data=row[1],numero=row[2],mensagem=row[3],dongle=row[4],status=row[5],dataenvio=row[6],tentativas=row[7],tipoenvio=row[8],agendado_ini=row[9],agendado_fim=row[10]))
						row = curs.fetchone()

				self.conn.commit()

			curs.close()

		return sms


	def smsNotSent(self):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sms=[]

		sql = "SELECT * FROM sms WHERE (status='A' OR status='F') AND dataenvio IS NULL ORDER BY data;"

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row = curs.fetchone()

				while row is not None:

					sms.append(dict(id=row[0],numero=row[2],mensagem=row[3],dongle=row[4],tentativas=row[7],dataini=row[9],datafim=row[10]))
					row = curs.fetchone()
			
			self.conn.commit()

		curs.close()

		return sms

	def isNewOptOut(self,novonumero):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sql = "SELECT * FROM optout where number='%s';" % novonumero

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				new=False
			else:

				new=True

			self.conn.commit()

		curs.close()

		return new
	
	def optOut(self,numero,dataini,datafim):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		optout=[]

		if dataini is not None:

			sql = "SELECT * FROM optout "

			where=""

			if numero!= "":

				where="WHERE number like '%%%s%%'" % numero

			if dataini != "" or datafim != "":

				if where == "":

					where = "WHERE "
				else:

					where += " AND "

				if dataini != "":

					where += "data >= '%s 00:00:00'" % dataini

				if datafim != "":

					if dataini != "":

						where+=" AND "

					where+="data <= '%s 23:59:59'" % datafim
		
			sql += "%s ORDER BY data;" % where

			#print sql

			try:
				curs.execute(sql)

			except:
				self.conn.rollback()

			else:

				if curs.rowcount>0:

					row = curs.fetchone()

					while row is not None:

						optout.append(dict(id=row[0],data=row[2],number=row[1],tipo=row[3]))
						row = curs.fetchone()

				self.conn.commit()

			curs.close()

		return optout

	def dropdown_Dongles(self):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sql = "SELECT dongle FROM dongles ORDER BY dongle;"

		dongles=[]

		try:
			curs.execute(sql)
		
		except:
			self.conn.rollback()
			
		else:

			if curs.rowcount>0:

				row = curs.fetchone()

				while row is not None:

					dongles.append(row[0])
					row = curs.fetchone()

			self.conn.commit()

		curs.close()

		return dongles


	def Dongles(self):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sql = "SELECT * FROM dongles ORDER BY dongle;"

		dongle=[]

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			ami=AMI()

			if curs.rowcount>0:

				row = curs.fetchone()

				while row is not None:

					data=ami.showDevices()
	
					if self.cfg.dongle_sinal=="1":

						sinal=getDongleSignal(data,row[1])
					else:

						sinal="ND"

					dongle.append(dict(id=row[0],dongle=row[1],ativo=row[2],number=row[3],imei=row[4],provider=row[5],model=row[6],sendsms=row[7],sinal="%s %%" % str(sinal)))
					row = curs.fetchone()

				self.conn.rollback()					

		curs.close()

		return dongle

	def Dongle_Control(self,dongle,dataini,datafim):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		dias=[]

		if dataini is not None:

			sql = "SELECT c.id,to_char(c.data,'DD/MM/YYYY'),d.dongle,d.number,c.smsenviados FROM controle AS c, dongles as D " 

			where="WHERE d.id=c.dongle_id"

			if dongle!= "":

				where+=" AND d.dongle='%s'" % dongle

			if dataini != "" or datafim != "":

				where += " AND "

				if dataini != "":

					where += "c.data >= '%s 00:00:00'" % dataini

				if datafim != "":

					if dataini != "":

						where+=" AND "

					where+="c.data <= '%s 23:59:59'" % datafim
		
			sql += " %s ORDER BY c.data,d.dongle;" % where
			#print sql

			try:
				curs.execute(sql)

			except:
				self.conn.rollback()

			else:

				if curs.rowcount>0:

					row = curs.fetchone()

					while row is not None:

						dias.append(dict(id=row[0],data=row[1],dongle=row[2],dongle_number=row[3],smsenviados=row[4]))
						row = curs.fetchone()

				self.conn.commit()

			curs.close()

		return dias


	def infoDongle(self,dongle):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs2 = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sql = "SELECT id,dongle,sendsms FROM dongles WHERE dongle='%s';" % dongle

		dongle={}

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row = curs.fetchone()

				if row[2]:

					try:
						curs2.execute("SELECT * FROM controle WHERE dongle_id=%i AND data='%s %s';" % (row[0],time.strftime("%Y-%m-%d"),"00:00:00"))

					except:
						dongle['id']=row[0]
						dongle['dongle']=row[1]
						dongle['valid']=True
						
						self.conn.rollback()
					else:
				
						if curs2.rowcount==0:

							dongle['id']=row[0]
							dongle['dongle']=row[1]
							dongle['valid']=True
						else:

							row2=curs2.fetchone()

							if row2[3] < int(self.cfg.sms_diario):
								dongle['id']=row[0]
								dongle['dongle']=row[1]
								dongle['valid']=True
							else:

								dongle['valid']=False
								dongle['errMotivo']="Dongle com envio esgotado para esta data."

							self.conn.commit()
				else:

					dongle['valid']=False
					dongle['errMotivo']="Dongle com envio desativado."
			else:

				dongle['valid']=False
				dongle['errMotivo']="Dongle inexistente."
		
			self.conn.commit()

		curs2.close()
		curs.close()

		return dongle

	def getDongle(self,dongle):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sql = "SELECT id,dongle FROM dongles WHERE dongle='%s';" % dongle

		retorno={}

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			row = curs.fetchone()

			retorno['id']=row[0]
			retorno['dongle']=row[1]
			retorno['sent']=0

		curs.close()

		return retorno



	def statusDongles(self):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs2 = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		dongle=[]

		sql = "SELECT * FROM dongles WHERE sendsms=True ORDER BY dongle;"

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row = curs.fetchone()

				while row is not None:

					try:
						curs2.execute("SELECT * FROM controle WHERE dongle_id=%i AND data='%s %s';" % (row[0],time.strftime("%Y-%m-%d"),"00:00:00"))

					except:
						dongle.append(dict(id=row[0],dongle=row[1],total=0))
						self.conn.rollback()

					else:

						total = 0

						if curs2.rowcount>0:

							row2 = curs2.fetchone()
							total = row2[3]
	
							dongle.append(dict(id=row[0],dongle=row[1],total=total))
						else:

							dongle.append(dict(id=row[0],dongle=row[1],total=0))

						self.conn.commit()

					row = curs.fetchone()

			self.conn.commit()

		curs2.close()
		curs.close()

		return dongle

	def getDongleToSMS(self):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs2 = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sql = "SELECT * FROM dongles WHERE sendsms=True ORDER BY dongle;"

		dongle={}

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row = curs.fetchone()

				while row is not None:

					try:
						curs2.execute("SELECT * FROM controle WHERE dongle_id=%i AND data='%s %s';" % (row[0],time.strftime("%Y-%m-%d"),"00:00:00"))

					except:
						dongle['id']=row[0]
						dongle['dongle']=row[1]
						self.conn.rollback()
						break
	
					else:

						max = int(self.cfg.sms_diario)

						if curs2.rowcount==0:

							dongle['id']=row[0]
							dongle['dongle']=row[1]

						else:
						
							row2 = curs.fetchone()
							total = row2[3]

							if total<max:

								max=total
								dongle['id']=row[0]
								dongle['dongle']=row[1]
					
						self.conn.commit()

					row = curs.fetchone()

			self.conn.commit()

		curs2.close()
		curs.close()

		return dongle

	def getDonglesToSMS(self):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs2 = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		max = int(self.cfg.sms_diario)

		sql = "SELECT * FROM dongles WHERE sendsms=True ORDER BY dongle;"

		dongles=[]

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row = curs.fetchone()

				while row is not None:

					dongle={}

					try:
						curs2.execute("SELECT * FROM controle WHERE dongle_id=%i AND data='%s %s';" % (row[0],time.strftime("%Y-%m-%d"),"00:00:00"))

					except:
						dongle['id']=row[0]
						dongle['dongle']=row[1]
						dongle['sent']=0
						dongles.append(dongle)

						self.conn.rollback()	
					else:

						if curs2.rowcount==0:

							dongle['id']=row[0]
							dongle['dongle']=row[1]
							dongle['sent']=0
							dongles.append(dongle)
						else:

							row2 = curs2.fetchone()
							total = row2[3]

							if total<max:

								dongle['id']=row[0]
								dongle['dongle']=row[1]
								dongle['sent']=total
								dongles.append(dongle)

						self.conn.rollback()

					row = curs.fetchone()

			self.conn.commit()

		curs2.close()
		curs.close()

		return dongles

	def updateDongleStatus(self,id,strAtivo,strSendsms):
		
		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sendsms=False

		if strAtivo=="off":

			ativo=False
		else:

			ativo=True

		if strAtivo=="on" and strSendsms=="on":

			sendsms=True

		sql="UPDATE dongles SET ativo=%s, sendsms=%s WHERE id=%s;" % (ativo,sendsms,id)

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()
			
		else:
			self.conn.commit()

		curs.close()
	
			
	def updateDongleData(self,data,id):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs2 = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sql="SELECT * FROM controle WHERE data='%s' AND dongle_id=%s;" % (data,id)

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount==0:

				sql2="INSERT INTO controle (dongle_id,data,smsenviados) VALUES (%s,'%s',1);" % (id,data)

			else:

				row=curs.fetchone()
				ctrlid=row[0]
				smsenviados=row[3]+1

				sql2="UPDATE controle SET smsenviados=%s WHERE id=%s;" % (smsenviados,ctrlid)

			try:
				curs2.execute(sql2)

			except:
				self.conn.rollback()

			else:

				self.conn.commit()

		curs2.close()
		curs.close()
	
	def updateSMS(self,id,dongle,idSMS,tentativas,status):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sql = "UPDATE sms SET dongle='%s',status='%s',sent_queue_id='%s',tentativas=%i WHERE id='%s';" % (dongle,status,idSMS,tentativas,id)

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:
			self.conn.commit()

		curs.close()

	def updateSMSStatus(self,idSMS,device,status):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		if status=="ok":

			sql = "UPDATE sms SET status='E',dataenvio='%s' WHERE sent_queue_id='%s' AND dongle='%s';" % (time.strftime("%Y-%m-%d %H:%M:%S"),idSMS,device)

			try:
				curs.execute(sql)

			except:
				self.conn.rollback()

			else:
				self.conn.commit()

			curs.close()

	def lockSMS(self,status,idSMS):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sql = "UPDATE sms SET status='%s',dataprocessado='%s' WHERE id=%s;" % (status,time.strftime("%Y-%m-%d %H:%M:%S"),idSMS)

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:
			self.conn.commit()

		curs.close()

	def optoutNumber(self,numero):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sql = "SELECT * FROM optout WHERE number='%s' or number='%s' or number='%s' or number='%s';" % (numero,self.cfg.DDDLOCAL+numero,"+55%s" % numero,"+55%s%s" % (self.cfg.DDDLOCAL,numero))

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount==0:

				self.conn.commit()
				curs.close()
				return False

			else:

				self.conn.commit()

		curs.close()

		return True

	def nextDongle(self,devices):

		maior=99999

		retorno={}

		for device in devices:

			max = int(self.cfg.sms_diario)

			if device['sent'] < max:

				if device['sent']==0:

					retorno['id']=device['id']
					retorno['dongle']=device['dongle']
					retorno['sent']=device['sent'] + 1
					break

			if device['sent'] < maior:

					maior=device['sent']

					retorno['id']=device['id']
					retorno['dongle']=device['dongle']
					retorno['sent']=device['sent'] + 1

		return retorno

	def resumoDia(self,data):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		Total=0
		Aguardando=0
		NaFila=0
		Enviado=0
		Cancelado=0
		Vencido=0
		OptOut=0


		sqlTotal="select count (id) from sms where data>='%s 00:00:00' and data<='%s 23:59:59'" % (data,data)

		try:
			curs.execute(sqlTotal)

		except:
			self.conn.rollback()
		
		else:

			if curs.rowcount>0:

				row=curs.fetchone()
				Total=row[0]

			self.conn.commit()


		sqlAguardando="select count (id) from sms where data>='%s 00:00:00' and data<='%s 23:59:59' and status='A'" % (data,data)

		try:
			curs.execute(sqlAguardando)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row=curs.fetchone()
				Aguardando=row[0]

			self.conn.commit()
		
	

		sqlNaFila="select count (id) from sms where data>='%s 00:00:00' and data<='%s 23:59:59' and status='F'" % (data,data)

		try:
			curs.execute(sqlNaFila)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row=curs.fetchone()
				NaFila=row[0]

			self.conn.commit()
	

		sqlEnviado="select count (id) from sms where data>='%s 00:00:00' and data<='%s 23:59:59' and status='E'" % (data,data)

		try:
			curs.execute(sqlEnviado)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row=curs.fetchone()
				Enviado=row[0]

			self.conn.commit()
		


		sqlCancelado="select count (id) from sms where data>='%s 00:00:00' and data<='%s 23:59:59' and status='C'" % (data,data)

		try:
			curs.execute(sqlCancelado)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row=curs.fetchone()
				Cancelado=row[0]

			self.conn.commit()


	
		sqlVencido="select count (id) from sms where data>='%s 00:00:00' and data<='%s 23:59:59' and status='V'" % (data,data)

		try:
			curs.execute(sqlVencido)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row=curs.fetchone()
				Vencido=row[0]

			self.conn.commit()



		sqlOptout="select count (id) from sms where data>='%s 00:00:00' and data<='%s 23:59:59' and status='O'" % (data,data)

		try:
			curs.execute(sqlOptout)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row=curs.fetchone()
				Optout=row[0]

			self.conn.commit()

		curs.close()

		dia={}
		dia['data']=data
		dia['total']=Total
		dia['aguardando']=Aguardando
		dia['nafila']=NaFila
		dia['enviado']=Enviado
		dia['cancelado']=Cancelado
		dia['vencido']=Vencido
		dia['optout']=Optout

		return dia

	def getSMSStat(self,dataini,datafim):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		Total=0
		Aguardando=0
		NaFila=0
		Enviado=0
		Cancelado=0
		Vencido=0
		OptOut=0


		sqlTotal="select count (id) from sms where data>='%s 00:00:00' and data<='%s 23:59:59'" % (dataini,datafim)

		try:
			curs.execute(sqlTotal)

		except:
			self.conn.rollback()
		
		else:

			if curs.rowcount>0:

				row=curs.fetchone()

				if row[0]>0:

					Total=float(row[0])
				else:

					Total=0

			self.conn.commit()


		sqlAguardando="select count (id) from sms where data>='%s 00:00:00' and data<='%s 23:59:59' and status='A'" % (dataini,datafim)

		try:
			curs.execute(sqlAguardando)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row=curs.fetchone()

				if row[0]==0:
					
					Aguardando=0
					percAguardando=0
				else:

					Aguardando=row[0]
					percAguardando=row[0]/Total

			self.conn.commit()
		
	

		sqlNaFila="select count (id) from sms where data>='%s 00:00:00' and data<='%s 23:59:59' and status='F'" % (dataini,datafim)

		try:
			curs.execute(sqlNaFila)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row=curs.fetchone()

				if row[0]==0:

					NaFila=0
					percNaFila=0
				else:
				
					NaFila=row[0]
					percNaFila=row[0]/Total

			self.conn.commit()
	

		sqlEnviado="select count (id) from sms where data>='%s 00:00:00' and data<='%s 23:59:59' and status='E'" % (dataini,datafim)

		try:
			curs.execute(sqlEnviado)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row=curs.fetchone()
				if row[0]==0:

					Enviado=0
					percEnviado=0
				else:

					Enviado=row[0]
					percEnviado=row[0]/Total

			self.conn.commit()
		


		sqlCancelado="select count (id) from sms where data>='%s 00:00:00' and data<='%s 23:59:59' and status='C'" % (dataini,datafim)

		try:
			curs.execute(sqlCancelado)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row=curs.fetchone()

				if row[0]==0:
				
					Cancelado=0
					percCancelado=0
				else:

					Cancelado=row[0]
					percCancelado=row[0]/Total

			self.conn.commit()


	
		sqlVencido="select count (id) from sms where data>='%s 00:00:00' and data<='%s 23:59:59' and status='V'" % (dataini,datafim)

		try:
			curs.execute(sqlVencido)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row=curs.fetchone()

				if row[0]==0:

					Vencido=0
					percVencido=0

				else:
				
					Vencido=row[0]
					percVencido=row[0]/Total

			self.conn.commit()



		sqlOptout="select count (id) from sms where data>='%s 00:00:00' and data<='%s 23:59:59' and status='O'" % (dataini,datafim)

		try:
			curs.execute(sqlOptout)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row=curs.fetchone()

				if row[0]==0:

					Optout=0
					percOptout=0
				else:

					Optout=row[0]
					percOptout=row[0]/Total

			self.conn.commit()

		curs.close()

		dados=[]

		dados.append(dict(label="Aguardando",perc=float("%.2f" % (percAguardando*100)),total=Aguardando))
		dados.append(dict(label="Cancelados",perc=float("%.2f" % (percCancelado*100)),total=Cancelado))
		dados.append(dict(label="Enviados",perc=float("%.2f" % (percEnviado*100)),total=Enviado))
		dados.append(dict(label="Na Fila",perc=float("%.2f" % (percNaFila*100)),total=NaFila))
		dados.append(dict(label="Opt-out",perc=float("%.2f" % (percOptout*100)),total=Optout))
		dados.append(dict(label="Vencidos",perc=float("%.2f" % (percVencido*100)),total=Vencido))

		return (Total,dados)

	########## Groups ##########

	def groups(self):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		groups=[]

		sql = "SELECT * FROM groups"

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row = curs.fetchone()

				while row is not None:

					memberlink='<a href="/group_members/%i">Membros</a>' % row[0]
					deletelink='<a href="#" onclick="confirmDeleteGroup(%i);return false;"><img src="/static/delete.png"></a>' % row[0]

					groups.append(dict(id=row[0],created=row[1].strftime("%Y-%m-%d %H:%M:%S"),last_update=row[2].strftime("%Y-%m-%d %H:%M:%S"),name=row[3],membersnum=row[4],memberlink=memberlink,deletelink=deletelink))
					row = curs.fetchone()

			self.conn.commit()

		curs.close()
		
		return groups

	def membersGroup(self,codGrupo):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs2 = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		name=""
		members=[]

		sql = "SELECT * FROM groupitems WHERE group_id=%s;" % codGrupo
		sql2 = "SELECT name FROM groups WHERE id=%s;" % codGrupo

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row = curs.fetchone()

				while row is not None:

					deletelink='<a href="#" onclick="confirmDeleteGroupItem(%i,%i);return false;"><img src="/static/delete.png"></a>' % (row[0],row[3])

					members.append(dict(id=row[0],name=row[2],number=row[1],deletelink=deletelink))
					row = curs.fetchone()

			self.conn.commit()

			try:
				curs2.execute(sql2)

			except:
				self.conn.rollback()

			else:

				if curs2.rowcount>0:

					row=curs2.fetchone()

					name=row[0]

				self.conn.commit()

			curs2.close()

		curs.close()
		
		return name,members



	def deleteGroup(self,codGrupo):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs2 = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		retorno = False

		sql = "DELETE FROM groupitems WHERE group_id=%s;" % codGrupo
		sql2 = "DELETE FROM groups WHERE id=%s;" % codGrupo

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			self.conn.commit()

			try:
				curs2.execute(sql2)

			except:
				self.conn.rollback()

			else:

				retorno = True
				self.conn.commit()

			curs2.close()

		curs.close()
		
		return retorno

	def deleteGroupItems(self,codGrupo):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		retorno = False

		sql = "DELETE FROM groupitems WHERE group_id=%i;" % codGrupo

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			self.conn.commit()

			retorno = True

		curs.close()
		
		return retorno

	def deleteGroupItem(self,id):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		retorno = False

		sql = "DELETE FROM groupitems WHERE id=%s;" % id

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			self.conn.commit()

			retorno = True

		curs.close()
		
		return retorno

	def getGroupMembers(self,group_name):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs2 = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		group_id=0
		members=[]

		sql = "SELECT id FROM groups WHERE name='%s';" % group_name

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			if curs.rowcount>0:

				row = curs.fetchone()
				group_id = row[0]

				sql2 = "SELECT number,name FROM groupitems WHERE group_id=%s;" % group_id
				
				self.conn.commit()
		
				try:
					curs2.execute(sql2)

				except:
					self.conn.rollback()

				else:

					if curs2.rowcount>0:

						row=curs2.fetchone()

						while row is not None:

							members.append([row[0],row[1]])
							row = curs2.fetchone()

				curs2.close()

		curs.close()
		
		return members


	def getGroupMembersNum(self,group_id):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sql = "SELECT membersnum FROM groups WHERE id=%s;" % group_id

		number = 0

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			self.conn.commit()

			if curs.rowcount>0:

				row=curs.fetchone()
				number=row[0]

		curs.close()
		
		return number


	def getGroupCode(self,name):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sql = "SELECT * FROM groups WHERE name='%s';" % name

		code = 0

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			self.conn.commit()

			if curs.rowcount>0:

				row=curs.fetchone()
				code=row[0]

		curs.close()
		
		return code

	def getGroupItemCode(self,number,group_id):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sql = "SELECT * FROM groupitems WHERE number='%s' and group_id=%s;" % (number,group_id)

		code = 0

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			self.conn.commit()

			if curs.rowcount>0:

				row=curs.fetchone()
				code=row[0]

		curs.close()
		
		return code


	def createGroup(self,name):

		code=0

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs2 = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sql = "INSERT INTO groups (name,membersnum) VALUES ('%s',0);" % name 
		sql2 = "SELECT * from groups WHERE name='%s';" % name 

		#print sql
		#print sql2

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

		else:

			self.conn.commit()

			try: 

				curs2.execute(sql2)
			except:
				self.conn.rollback()

			else:

				if curs2.rowcount>0:

					row=curs2.fetchone()
					code=row[0]

				self.conn.commit()

			curs2.close()

		curs.close()
		
		return code

	def updateGroupMembersNumber(self,codGrupo,membros):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		retorno=True

		sql = "UPDATE groups SET membersnum = %s WHERE id = %s;" % (membros,codGrupo) 

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()

			retorno=False

		else:

			self.conn.commit()

		curs.close()
		
		return retorno

	def insertGroupMember(self,codGrupo,line):

		retorno=True

		dados=line.split(',')

		dados[0]=(dados[0].replace("-","").replace(".","").replace("(","").replace(")","").replace(" ",""))
		
		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
	
		sql = "INSERT INTO groupitems (number,name,group_id) VALUES ('%s','%s',%i);" % (dados[0],dados[1],codGrupo)

		try:
			curs.execute(sql)

		except:
			self.conn.rollback()
			retorno=False

		curs.close()
		
		return retorno

	def dropdown_Groups(self):

		curs = self.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)

		sql = "SELECT name FROM groups ORDER BY name;"

		groups=[]

		try:
			curs.execute(sql)
		
		except:
			self.conn.rollback()
			
		else:

			if curs.rowcount>0:

				row = curs.fetchone()

				while row is not None:

					groups.append(row[0])
					row = curs.fetchone()

			self.conn.commit()

		curs.close()

		return groups



def getDongleSignal(data,device):

	mgrdictev=ManagerDictEventsType(data,'Device',device)

	for event in mgrdictev.events:

		rssi=int(event['RSSI'].split(',')[0])*3

	return rssi


