#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
		config.py
		Classe da configuração do sistema
		2015/01/26
		Marcelo Hartmann Terres <mhterres@gmail.com>
"""

import os
import sys
import ConfigParser

class Config:

	def __init__(self):

		self.description = 'Configurations'

		configuration = ConfigParser.RawConfigParser()

		configuration = ConfigParser.RawConfigParser()
		configuration.read('/etc/websms/sms.conf')

		# geral
		self.sms_diario=configuration.get('geral','sms_diario')
		self.horario_envio_workdays=configuration.get('geral','horario_envio_workdays')
		self.horario_envio_weekend=configuration.get('geral','horario_envio_weekend')
		self.dias_da_semana_envio=configuration.get('geral','dias_da_semana_envio')
		self.tentativas_envio=configuration.get('geral','tentativas_envio')
		self.debug=configuration.get('geral','debug')
		self.url=configuration.get('geral','url')
		self.DDDLOCAL=configuration.get('geral','DDDLOCAL')
		self.dongle_sinal=configuration.get('geral','dongle_sinal')
		self.servicepath=configuration.get('geral','servicepath')

		# paths
		self.htmlpath=configuration.get('paths','htmlpath')

		# database
		self.db_type=configuration.get('database','db_type')
		self.db_host=configuration.get('database','db_host')
		self.db_port=configuration.get('database','db_port')
		self.db_name=configuration.get('database','db_name')
		self.db_user=configuration.get('database','db_user')
		self.db_pwd=configuration.get('database','db_pwd')

		# ami
		self.ami_host=configuration.get('ami','ami_host')
		self.ami_port=configuration.get('ami','ami_port')
		self.ami_user=configuration.get('ami','ami_user')
		self.ami_pwd=configuration.get('ami','ami_pwd')

		# email
		self.send_email=configuration.get('email','send_email')
		self.email_send=configuration.get('email','email_send')
		self.email_dest=configuration.get('email','email_dest')
		self.email_host=configuration.get('email','email_host')
		self.send_email_alerta=configuration.get('email','send_email_alerta')
		self.email_alerta_send=configuration.get('email','email_alerta_send')
		self.email_alerta_dest=configuration.get('email','email_alerta_dest')

		self.sendweekdays=self.__SendWeekDays()

	def __SendWeekDays(self):

		weekday=[]
		# segunda (0)
		weekday.append(self.dias_da_semana_envio[1:2])
		weekday.append(self.dias_da_semana_envio[2:3])
		weekday.append(self.dias_da_semana_envio[3:4])
		weekday.append(self.dias_da_semana_envio[4:5])
		weekday.append(self.dias_da_semana_envio[5:6])
		weekday.append(self.dias_da_semana_envio[6:7])
		weekday.append(self.dias_da_semana_envio[0:1])

		return (weekday)


