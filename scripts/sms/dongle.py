#!/usr/bin/python
# -*- coding: utf-8 -*-

# dongle.py
#
# Class to access and use Dongles
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-03-19
#

import time

from config import Config
from ami import AMI
from dictio import ManagerDict
from dictio import ManagerDictEvents
from dbpgsql import DBPgsql
from sockets import recv_basic
from sockets import recv_timeout


import psycopg2
import psycopg2.extras

class Dongle():

	def __init__(self):

		cfg = Config()
		ami = AMI()
		db = DBPgsql(cfg)
		self.ami=ami
		self.cfg=cfg
		self.db=db

	def donglesUpdate(self):

		self.ami.socket.send('Action: DongleShowDevices\n\n')

		data=recv_timeout(self.ami.socket,2)

		mgrdictev=ManagerDictEvents(data,'DongleDeviceEntry') 

		self.data=data
		self.dongles=mgrdictev

	def getDongles(self):

		devices=[]

		for dongle in self.dongles.events:

			devices.append(dongle['Device'].strip().strip("\r").strip("\n"))

		self.devices=devices

		return devices

	def getDongleInfo(self,devices):

		for device in devices:

			dictDevice=self._getDictDevice(self.data,device)

			if len(dictDevice)>0:

				if self.cfg.debug=="1":

					print "Device: %s" % device
					print "rssi: %s" % dictDevice['rssi']
					print "channel: %s" % dictDevice['channel']
					print "operadora: %s" % dictDevice['operadora']
					print "imei: %s" % dictDevice['imei']
					print "modem: %s" % dictDevice['modem']
					print "number: %s" % dictDevice['number']
					print "group: %s" % dictDevice['grupo']
					print "state: %s" % dictDevice['state']
					print "registrogsm: %s" % dictDevice['registrogsm']
					print ""

			return dictDevice

	def _getDictDevice(self,data,device): 
 
		mgrdictev=self.dongles
 
		stats = {} 

		if mgrdictev.isvalid:

			for event in mgrdictev.events: 

				if event['Device'].strip()==device.strip():
 
					modem_manufacturer = event['Manufacturer'].strip().strip("\n").strip("\r")
					modem_model = event['Model'].strip().strip("\n").strip("\r")
					operadora = event['ProviderName'].strip().strip("\n").strip("\r")
					imei = event['IMEISetting'].strip().strip("\n").strip("\r")
					number = event['SubscriberNumber'].strip().strip("\n").strip("\r")
					grupo = event['Group'].strip().strip("\n").strip("\r")
					state = event['State'].strip().strip("\n").strip("\r")
					registrogsm = event['GSMRegistrationStatus'].strip().strip("\n").strip("\r")

					stats.update({"rssi":event['RSSI'].split(',')[0]}) 
					stats.update({"channel":device}) 
					stats.update({"operadora":operadora.replace("\r","")}) 
					stats.update({"imei":imei.replace("\r","")}) 
					stats.update({"modem":modem_manufacturer.replace("\r","")+" - "+modem_model.replace("\r","")}) 
					stats.update({"number":number.replace("\r","")}) 
					stats.update({"grupo":grupo.replace("\r","")}) 
					stats.update({"state":state.replace("\r","")}) 
					stats.update({"registrogsm":registrogsm.replace("\r","")}) 
		return stats 

	def getDongleSmsTotal(self,dongle):

		print "ok"

	def updateDB(self,data):

		if not (self.deviceExists(data['channel'])):

			sql="INSERT INTO dongles (dongle,number,imei,provider,model) VALUES ('%s','%s','%s','%s','%s');" % (data['channel'],data['number'],data['imei'],data['operadora'],data['modem'])

			if self.cfg.debug=="1":

				print "Inserting dongle."
				print "sql: %s" % sql

		else:

			sql="UPDATE dongles SET number='%s',imei='%s',provider='%s',model='%s',ativo=True WHERE dongle='%s';" % (data['number'],data['imei'],data['operadora'],data['modem'],data['channel'])

			if self.cfg.debug=="1":

				print "Updating dongle."
				print "sql: %s" % sql

		curs = self.db.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs.execute(sql)
		self.db.conn.commit()
		curs.close()

	def deviceExists(self,device):

		sql="SELECT * from dongles where dongle='%s';" % device

		if self.cfg.debug=='1':

			print "Searching for device %s." % device
			print "sql: %s" % sql

		curs = self.db.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs.execute(sql)

		if curs.rowcount > 0:

			curs.close()
			return True
	
		curs.close()
		return False
	
	def verifyDB(self):

		sql="SELECT dongle from dongles;"

		if self.cfg.debug=='1':

			print "Verifying Database."
			print "sql: %s" % sql

		curs = self.db.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs.execute(sql)

		if curs.rowcount > 0:

			row=curs.fetchone()

			for row in curs:

				if row['dongle'] in self.devices:

					if self.cfg.debug=="1":

						print "Dongle %s found." % row['dongle']
				else:

					if self.cfg.debug=="1":

						print "Dongle %s not found. It'll be disabled." % row['dongle']
	
					self.disable(row['dongle'])

		curs.close()
		return True
		

	def disable(self,dongle):

		sql="UPDATE dongles set ativo=False where dongle='%s';" % dongle

		if self.cfg.debug=='1':

			print "Disabling dongle %s." %dongle
			print "sql: %s" % sql

		curs = self.db.conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curs.execute(sql)
		curs.close()

	def enableSMS(self,dongle):

		print "ok"

	def disableSMS(self,dongle):

		print "ok"

	def getDongleSignal(data,device):

		mgrdictev=ManagerDictEvents(data,'Device',device)

		for event in mgrdictev.events:

			rssi=int(event['RSSI'].split(',')[0])*3

		print "Device: %s" % device
		print "rssi: %s" % str(rssi)

