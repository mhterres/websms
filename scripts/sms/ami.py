#!/usr/bin/python
# -*- coding: utf-8 -*-

# ami.py
#
# Class to use Asterisk Manager Interface
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-03-18
#

import os
import sys
import time
import socket

from unidecode import unidecode

from config import Config
from dictio import ManagerDict
from sockets import recv_basic
from sockets import recv_timeout

class AMI:

	def __init__(self,events="off"):

		cfg = Config()

		s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		s.connect((cfg.ami_host,int(cfg.ami_port)))

		s.send('Action: Login\nUsername: '+cfg.ami_user+'\nSecret: '+cfg.ami_pwd+'\nEvents: %s\n\n' % events)
		time.sleep (0.1)
		data = s.recv(2048)

		self.socket = s

	def sendSMS(self,dongle,numero,message):

		amicmd='Action: DongleSendSMS\nDevice: %s\nNumber: %s\nMessage: %s\n\n' % (dongle,numero,message.replace("\r","").replace("\n",""))
		print amicmd
		self.socket.send('Action: DongleSendSMS\nDevice: %s\nNumber: %s\nMessage: %s\n\n' % (dongle,numero,message.replace("\r","").replace("\n","")))
		time.sleep (0.1)
		data = self.socket.recv(2048)

		#print "Data: %s" % data

		dictio=ManagerDict(data)

		response=dictio.getitem('Response').replace("\\","").strip()
		id=dictio.getitem('ID').replace("\\","").strip()

		print "Response: %s" % response
		#print "Message: %s" % message.replace("\r","").replace("\n","")
		print "ID: %s" % id

		if response=="Success":

			idSMS=id
		else:

			idSMS=""

		return idSMS

	def showDevices(self):

			self.socket.send('Action: DongleShowDevices\n\n')
			time.sleep (1)
			data = self.socket.recv(65536)

			return data
