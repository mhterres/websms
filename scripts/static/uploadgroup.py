#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Cria/Edita grupo de envio
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-08-19
#

import sys
import time
import cgi, os
import cgitb; cgitb.enable()
import datetime

sys.path.insert(1,'/usr/lib/python2.7/dist-packages/sms')

from config import Config
from dbpgsql import DBPgsql

cfg=Config()
DB=DBPgsql(cfg)

form = cgi.FieldStorage()

fileitem = form['filename']
name = form['name'].value

f=open('./header.html', 'r')
header=f.read()
f.close()

f=open('./footer.html', 'r')
footer=f.read()
f.close()

if fileitem.filename:

	processa=True

	codGrupo=DB.getGroupCode(name)

	if codGrupo>0:

		status="editado"
		output=header.replace("%TITLE%","WebSMS").replace("%SUBTITLE%","Edi&ccedil;&atilde;o de grupo de envio")

		processa=DB.deleteGroupItems(codGrupo)
	else:

		status="criado"
		output=header.replace("%TITLE%","WebSMS").replace("%SUBTITLE%","Cria&ccedil;&atilde;o de grupo de envio")

		codGrupo=DB.createGroup(name)

	membros=0

	if processa and codGrupo > 0:

		fn = os.path.basename(fileitem.filename)
		open('/tmp/' + fn, 'wb').write(fileitem.file.read())
	
		output+="  <p>Grupo %s %s com sucesso." % (name,status)
		output+="	  <hr>"
		output+="  	</body>"
		output+=" 	</html>"

		with open('/tmp/%s' % fn) as f:

			for line in f:

				line=(line.replace("\r","").replace("\n","")).strip()

				if len(line)>0:
			
					if DB.insertGroupMember(codGrupo,line):

						membros=membros+1

		DB.updateGroupMembersNumber(codGrupo,membros)

		output+="  <p>%i membros adicionados." % membros
		output+="	  <hr>"
		output+="  	</body>"
		output+=" 	</html>"
	else:

		output+="  <p>Erro na cria&ccedil;&atilde;o/edi&ccedil;&atilde;o do grupo %s." % name
		output+="	  <hr>"
		output+="  	</body>"
		output+=" 	</html>"
		

output+="	  <hr>"
output+="    <table border=0 style='border: 0px';>"
output+="     <tr style='text-align: right;' width='100%'>"
output+="      <td style='text-align: right;' width='100%' colspan=2>"
output+="       <input type='button' onClick='location.href = \"/group_upload/\";' class='button tiny' value='Cria/Edita novo grupo de envio'>"
output+="      </td>"
output+="     </tr>"
output+="    </table>"

#output+="		  <a href='/envialote/'>Novo envio de SMS em lote</a>"
output+=footer

print "Content-type:text/html\r\n\r\n"
print output

