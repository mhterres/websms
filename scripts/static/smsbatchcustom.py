#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Envia SMS em lote
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-05-07
#

import sys
import time
import cgi, os
import cgitb; cgitb.enable()
import datetime
import subprocess

sys.path.insert(1,'/usr/lib/python2.7/dist-packages/sms')

from config import Config
from dbpgsql import DBPgsql
from defs import remover_acentos

cfg=Config()
DB=DBPgsql(cfg)


form = cgi.FieldStorage()

fileitem = form['filename']
mensagem = form['mensagem'].value
dataini =  form['start'].value
horaini = form['timestart'].value
datafim =  form['end'].value
horafim = form['timeend'].value

agendado_ini=""
agendado_fim=""

if dataini != "":

  if horaini != "":

    agendado_ini="%s %s:00" % (dataini,horaini.replace(" ",""))
  else:

    agendado_ini="%s 00:00:00" % dataini

  dt=datetime.datetime.strptime(agendado_ini, "%Y-%m-%d %H:%M:%S")
  dtini=time.mktime(dt.timetuple())

if datafim != "":

  if horafim != "":

    agendado_fim="%s %s:00" % (datafim,horafim.replace(" ",""))
  else:

    agendado_fim="%s 00:00:00" % datafim

  dt=datetime.datetime.strptime(agendado_fim, "%Y-%m-%d %H:%M:%S")
  dtfim=time.mktime(dt.timetuple())

f=open('./header.html', 'r')
header=f.read()
f.close()

f=open('./footer.html', 'r')
footer=f.read()
f.close()

output=header.replace("%TITLE%","WebSMS").replace("%SUBTITLE%","Envio de SMS em lote personalizado")

ok=True

if agendado_fim != "":

  if agendado_ini == "":

    ok=False
    output+="  <p>Data inicial n&atilde;o informada."
  else:

    if dtfim<dtini:

      ok=False
      output+="  <p>Data final menor que a data inicial."

if ok:
	
	if fileitem.filename:

		fn = os.path.basename(fileitem.filename)
		open('/tmp/' + fn, 'wb').write(fileitem.file.read())
		subprocess.Popen("filename=/tmp/%s && type=`file -i $filename | cut -f 2 -d '='` && iconv -f $type -t utf-8 $filename -o /tmp/processed_%s" % (fn,fn) ,shell=True, stdout=subprocess.PIPE).stdout.read()

		output+="  <p>SMS Enviado com sucesso."
		output+="	  <hr>"
		output+="  	</body>"
		output+=" 	</html>"
		output+="   <p><b>Celulares de destino</b><br>"

		with open('/tmp/processed_%s' % fn) as f:

			for line in f:

				if len(line.strip())>0:

					line=(line.replace("\r","").replace("\n","")).strip()
	                                line=line.replace(";",",")
        	                        line=line.replace("'","")

					if line.find(",") > -1:

						dados=line.split(",")
						numero=dados[0]
						nome=dados[1]
					else:

						numero=line
						nome=""
				
					mensagemnova=mensagem.replace("%NUMERO%",numero).replace("%NOME%",nome)


					if DB.insertSentSMS(time.strftime("%Y-%m-%d %H:%M:%S"),numero,remover_acentos(mensagemnova),"L",agendado_ini,agendado_fim):
        	                                output+=numero+" - OK <br>"
                        	        else:
                	                        output+=numero+" - FALHA <br>"

#output+="	  <hr>"
#output+="		  <a href='/envialotecustom/'>Novo envio de SMS em lote personalizado</a>"
output+="   <hr>"
output+="    <table border=0 style='border: 0px';>"
output+="     <tr style='text-align: right;' width='100%'>"
output+="      <td style='text-align: right;' width='100%' colspan=2>"
output+="       <input type='button' onClick='location.href = \"/envialotecustom/\";' class='button tiny' value='Envia Novo SMS em lote personalizado'>"
output+="      </td>"
output+="     </tr>"
output+="    </table>"

output+=footer

print "Content-type:text/html\r\n\r\n"
print output

