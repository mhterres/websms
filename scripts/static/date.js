jQuery(function($) {
	$("[name=src]").keydown(function() {
		$("[name=src_group]").val("__all__");
	});
	$("[name=dst]").keydown(function() {
		$("[name=dst_group]").val("__all__");
	});

	$("[name=src_group]").change(function() {
		$("[name=src]").val("");
	});
	$("[name=dst_group]").change(function() {
		$("[name=dst]").val("");
	});
//
//	$(".datepicker_ini").datepicker({
//		startDate:'1970-01-01',
//		createButton:false
//	}).dpSetEndDate((new Date()).asString());
//	$(".datepicker_fim").datepicker({
//		startDate:'1970-01-01',
//		createButton:false
//	}).dpSetEndDate((new Date()).asString());


	$(".datepicker_ini").change(function(ev) {
		$("select#shortcut").val("nil");
	});
	$(".datepicker_fim").change(function(ev) {
		$("select#shortcut").val("nil");
	});


	$(".datepicker_ini").each(function() {
		var dt = $(this);
		dt.next().click(function(ev) {
			dt.dpDisplay();
		});
		dt.dpSetOffset(dt.height()+7,0);// dt.width());
	});
	$(".datepicker_fim").each(function() {
		var dt = $(this);
		dt.next().click(function(ev) {
			dt.dpDisplay();
		});
		dt.dpSetOffset(dt.height()+7,0);// dt.width());
	});



	$("select#shortcut").change(function() {
		var selected = $(this).find(":selected");
		var d1 = new Date();
		var d2 = new Date();
		var fun = selected.val();
		var r = dateRanges[fun](d1,d2);
		if (r == null) {
			$(".datepicker_ini#start").val(d1.asString());
			$(".datepicker_fim#end").val(d2.asString());
		}
		else {
			$(".datepicker_ini#start").val('');
			$(".datepicker_fim#end").val('');
		}
	});

	$("input[name=grouping]").click(function() {
		if ($(":checked[name=grouping]").val() == "cost_center") {
			$(".order").hide();
			$(":checked[name=order]").removeAttr("checked");
		} else
			$(".order").show();
	});

	$(document).ready(function() {
		if ($(":checked[name=grouping]").val() == "cost_center") {
			$(".order").hide();
			$(":checked[name=order]").removeAttr("checked");
		}
	});

//	$("select#shortcut option:selected").trigger("click");
});
