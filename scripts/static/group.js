function validateFormGroupUpload()
{
  var filename = document.forms["uploadgroup"]["filename"].value;
  var name = document.forms["uploadgroup"]["name"].value;

  if (filename == null || filename == "") {
    alert("O arquivo deve ser informado.");
    return false;
  }

  if (name == null || name == "") {
    alert("O nome do grupo deve ser informado.");
    return false;
  }
  document.forms['uploadgroup'].submit();
  return true;
}

function validateNewGroup()
{
  var name = document.forms["newgroup"]["name"].value;

  if (name == null || name == "") {
    alert("O nome do grupo deve ser informado.");
    return false;
  }

  document.forms['newgroup'].submit();
  return true;
}

function validateNewGroupItem()
{
  var name = document.forms["newmember"]["name"].value;
  var number = document.forms["newmember"]["number"].value;

  if (name == null || name == "") {
    alert("O nome deve ser informado.");
    return false;
  }

  if (number == null || number == "") {
    alert("O número deve ser informado.");
    return false;
  }
	document.forms['newmember'].submit();
  return true;
}


function confirmDeleteGroup(id)  
{ 
  var resposta = confirm("Deseja remover esse grupo?"); 
 
  if (resposta == true)  
  { 
    window.location.href = "/delete_group/"+id;  
  } 
} 

function returnListGroup(id)
{

	window.location.href = "/group_list/"+id;  
}

function confirmDeleteGroupItem(id,group_id)  
{ 
  var resposta = confirm("Deseja remover esse membro do grupo?"); 
 
  if (resposta == true)  
  { 
    window.location.href = "/delete_groupitem/?id="+id+"&group_id="+group_id;  
  } 
} 

function returnListGroupMembers(id)
{

	window.location.href = "/group_members/"+id;  
}
