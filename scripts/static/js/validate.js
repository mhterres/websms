jQuery.fn.validate = function(func) {
	$(this).each(function() {
		this.validateFunc = func;
		$(this).change(function() {
			if (this.validateFunc())
				$(this).css("background","transparent");
			else
				$(this).css("background","red");
		});
		$(this).triggerHandler("change");
	});
};

jQuery.fn.validateAll = function(mark) {
	var valid = true;
	$(this).each(function() {
		func = this.validateFunc;
//		if (func)
//			alert("retornando "+func()+" para o campo "+$(this).attr("name"));
		if (!this.validateFunc)
			$(this).css("background","transparent");
		else if (this.validateFunc())
			$(this).css("background","transparent");
		else {
			valid = false;
			if (mark) $(this).css("background","red");
		}
	});
	return valid;
};

jQuery.fn.getInvalid = function() {
	var invalid = new Array();
	$(this).each(function() {
		if ((this.validateFunc) && (!this.validateFunc())) {
			invalid.push(this);
		}
	});
	return invalid;
}
