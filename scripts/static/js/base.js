jQuery(function($) {
	$("input").focus(function() {
		if ($(this).hasClass("inTable")) return;
		this.oldBorder = $(this).css("border");
		$(this).css("border","2px solid green");
	});
	$("input").blur(function() {
		if ($(this).hasClass("inTable")) return;
		$(this).css("border",this.oldBorder);
	});

});

function $BASE() {
	return $("base").attr("href");
}
