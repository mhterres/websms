dateRanges = new Object();
dateRanges.nil          = function(d1,d2) { return '' },
dateRanges.today        = function(d1,d2) {},
dateRanges.yesterday    = function(d1,d2) {
	d1.addDays(-1);
	d2.addDays(-1);
},
dateRanges.last7days    = function(d1,d2) {
	d1.addDays(-7);
},
dateRanges.last30days   = function(d1,d2) {
	d1.addDays(-30);
},
dateRanges.currentMonth = function(d1,d2) {
	d1.setDate(1);
},
dateRanges.lastMonth    = function(d1,d2) {
	d1.setDate(1);
	d1.addMonths(-1);
	d2.setDate(1);
	d2.addDays(-1);
},
dateRanges.currentYear  = function(d1,d2) {
	d1.setDate(1);
	d1.setMonth(0);
},
dateRanges.lastYear     = function(d1,d2) {
	d1.setDate(1);
	d1.setMonth(0);
	d1.addYears(-1);
	d2.setDate(1);
	d2.setMonth(0);
	d2.addDays(-1);
}
