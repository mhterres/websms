/*
 * Propus Prototype Extensions
 *
 * $Id$
 *
 * Marlon Dutra
 * http://www.propus.com.br
 * August 25th, 2007
 */

/*
 * Removes all children elements from an element
 */
Element.addMethods( {
	removeChildren: function(element)
	{
		for (var i = element.childNodes.length-1; i >= 0; i--)
		{
			element.removeChild(element.childNodes[i]);
		}
	}
});
