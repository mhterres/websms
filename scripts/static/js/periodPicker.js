$.fn.periodPicker = function() {
	// Usually applied on wrapper objects, i.e. a TR wrapping the actual input boxes
	// The outer element must wrap at least:
	//   an input box with name=start (followed by a button)
	//   an input box with name=end   (followed by a button)
	//   a select box with name=shortcut
	var shortcut = $(this).find("select[name=shortcut]");
	var start    = $(this).find("input[name=start]");
	var end      = $(this).find("input[name=end]");
	var inputs   = start.add(end);

	var opts = {
		startDate:'1970-01-01',
		createButton:false
	}
	var today = (new Date()).asString();

	inputs.datePicker(opts);
	inputs.dpSetEndDate(today);
	inputs.change(function(ev) {
		shortcut.val("nil");
	});
	inputs.each(function() {
		var dt = $(this);
		dt.next().click(function(ev) {
			dt.dpDisplay();
		});
		dt.dpSetOffset(dt.height()+7,0);// dt.width());
	});

	shortcut.change(function() {
		var selected = shortcut.find(":selected");
		var d1 = new Date();
		var d2 = new Date();
		var fun = selected.val();
		var r = dateRanges[fun](d1,d2);
		if (r == null) {
			start.val(d1.asString());
			end.val(d2.asString());
		}
		else {
			inputs.val('');
		}
	});
}
