function validateFormSmsBatch()
{
  var filename = document.forms["smsbatch"]["filename"].value;
  var mensagem = document.forms["smsbatch"]["mensagem"].value;

  if (filename== null || filename == "") {
    alert("O arquivo deve ser informado.");
    return false;
  }

  if (mensagem== null || mensagem == "") {
    alert("A mensagem deve ser informada.");
    return false;
  }
  document.forms['smsbatch'].submit();
  return true;
}

function validateFormSmsBatchCustomMsg()
{
  var filename = document.forms["smsbatchcustommsg"]["filename"].value;

  if (filename== null || filename == "") {
    alert("O arquivo deve ser informado.");
    return false;
  }

  document.forms['smsbatchcustommsg'].submit();
  return true;
}

function validateFormSmsGroup()
{
  var group = document.forms["smsgroup"]["grupo"].value;
  var mensagem = document.forms["smsgroup"]["mensagem"].value;

  if (group== null || group == "") {
    alert("O grupo deve ser informado.");
    return false;
  }

  if (mensagem== null || mensagem == "") {
    alert("A mensagem deve ser informada.");
    return false;
  }
  document.forms['smsgroup'].submit();
  return true;
}


function validateFormSmsOne()
{
  var numero = document.forms["smsone"]["numero"].value;
  var mensagem = document.forms["smsone"]["mensagem"].value;

  if (numero== null || numero == "") {
    alert("O numero deve ser informado.");
    return false;
  }

	if (isNaN(numero)) {
    alert("Numero invalido.");
    return false;
  }

  if (mensagem== null || mensagem == "") {
    alert("A mensagem deve ser informada.");
    return false;
  }
  document.forms['smsone'].submit();
  return true;
}



