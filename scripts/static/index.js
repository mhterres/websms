jQuery(function($) {
	$("[name=src]").keydown(function() {
		$("[name=src_group]").val("__all__");
	});
	$("[name=dst]").keydown(function() {
		$("[name=dst_group]").val("__all__");
	});

	$("[name=src_group]").change(function() {
		$("[name=src]").val("");
	});
	$("[name=dst_group]").change(function() {
		$("[name=dst]").val("");
	});

	$(".datepicker").datePicker({
		startDate:'1970-01-01',
		createButton:false
	}).dpSetEndDate((new Date()).asString());

	$(".datepicker").change(function(ev) {
		$("select#shortcut").val("nil");
	});

	$(".datepicker").each(function() {
		var dt = $(this);
		dt.next().click(function(ev) {
			dt.dpDisplay();
		});
		dt.dpSetOffset(dt.height()+7,0);// dt.width());
	});


	$("select#shortcut").change(function() {
		var selected = $(this).find(":selected");
		var d1 = new Date();
		var d2 = new Date();
		var fun = selected.val();
		var r = dateRanges[fun](d1,d2);
		if (r == null) {
			$(".datepicker#start").val(d1.asString());
			$(".datepicker#end").val(d2.asString());
		}
		else {
			$(".datepicker#start").val('');
			$(".datepicker#end").val('');
		}
	});

	$("input[name=grouping]").click(function() {
		if ($(":checked[name=grouping]").val() == "cost_center") {
			$(".order").hide();
			$(":checked[name=order]").removeAttr("checked");
		} else
			$(".order").show();
	});

	$(document).ready(function() {
		if ($(":checked[name=grouping]").val() == "cost_center") {
			$(".order").hide();
			$(":checked[name=order]").removeAttr("checked");
		}
	});

//	$("select#shortcut option:selected").trigger("click");
});
