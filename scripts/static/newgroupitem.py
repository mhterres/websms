#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Adiciona membro ao grupo de envio
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-08-21
#

import sys
import time
import cgi, os
import cgitb; cgitb.enable()
import datetime

sys.path.insert(1,'/usr/lib/python2.7/dist-packages/sms')

from config import Config
from dbpgsql import DBPgsql

cfg=Config()
DB=DBPgsql(cfg)

form = cgi.FieldStorage()

name = form['name'].value
number = form['number'].value
number = (number.replace("-","").replace(".","").replace("(","").replace(")","").replace(" ",""))
group_id = form['group_id'].value

f=open('./header.html', 'r')
header=f.read()
f.close()

f=open('./footer.html', 'r')
footer=f.read()
f.close()

codGroupItem=DB.getGroupItemCode(number,group_id)

output=header.replace("%TITLE%","WebSMS").replace("%SUBTITLE%","Adi&ccedil;&atilde;o de membro a grupo de envio")

if codGroupItem>0:

	output+="  <p>Telefone %s j&aacute; pertence ao grupo." % number
	output+="	  <hr>"
	output+="  	</body>"
	output+=" 	</html>"

else:

	members_num=DB.getGroupMembersNum(group_id)

	if DB.insertGroupMember(int(group_id),"%s,%s" % (number,name)):

		DB.updateGroupMembersNumber(group_id,members_num+1)

		output+="  <p>%s adicionado." % number
		output+="	  <hr>"
		output+="  	</body>"
		output+=" 	</html>"
	else:

		output+="  <p>Erro na adi&ccedil;&atilde;o do telefone %s ao grupo." % number
		output+="	  <hr>"
		output+="  	</body>"
		output+=" 	</html>"
		

output+="	  <hr>"
output+="    <table border=0 style='border: 0px';>"
output+="     <tr style='text-align: right;' width='100%'>"
output+="      <td style='text-align: right;' width='100%' colspan=2>"
output+="       <input type='button' onClick='location.href = \"/group_members/%s\";' class='button tiny' value='Voltar'>" % group_id
output+="      </td>"
output+="     </tr>"
output+="    </table>"


output+=footer

print "Content-type:text/html\r\n\r\n"
print output

