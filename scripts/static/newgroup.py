#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Cria grupo de envio
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-08-21
#

import sys
import time
import cgi, os
import cgitb; cgitb.enable()
import datetime

sys.path.insert(1,'/usr/lib/python2.7/dist-packages/sms')

from config import Config
from dbpgsql import DBPgsql

cfg=Config()
DB=DBPgsql(cfg)

form = cgi.FieldStorage()

name = form['name'].value

f=open('./header.html', 'r')
header=f.read()
f.close()

f=open('./footer.html', 'r')
footer=f.read()
f.close()

codGrupo=DB.getGroupCode(name)

output=header.replace("%TITLE%","WebSMS").replace("%SUBTITLE%","Cria&ccedil;&atilde;o de grupo de envio")

if codGrupo>0:

		output+="  <p>Grupo de envio %s j&aacute; existe." % name
		output+="	  <hr>"
		output+="  	</body>"
		output+=" 	</html>"
else:		

		codGrupo=DB.createGroup(name)

output+="	  <hr>"
output+="    <table border=0 style='border: 0px';>"
output+="     <tr style='text-align: right;' width='100%'>"
output+="      <td style='text-align: right;' width='100%' colspan=2>"
output+="       <input type='button' onClick='location.href = \"/group_list/\";' class='button tiny' value='Voltar'>"
output+="      </td>"
output+="     </tr>"
output+="    </table>"

output+=footer

print "Content-type:text/html\r\n\r\n"
print output

