#!/usr/bin/python
# -*- coding: utf-8 -*-

# smsverify.py
# Verifica se daemons do SMS estão rodando
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-05-27
#

import os
import sys
import time
import socket
import subprocess 

sys.path.insert(1,'/usr/lib/python2.7/dist-packages/sms')

from config import Config
from mail import Mail

cfg=Config()

host=socket.gethostname()

startCmd=cfg.servicepath
startParm="start"

def verifySMSStatus(processname):

  tmp = os.popen("ps -Af").read()
  proccount = tmp.count(processname)

  if proccount > 0:

    return True

  return False

cfg=Config()
mail=Mail()

processes=['websms.py','smsstatus.py']

for process in processes:

	if not verifySMSStatus(process):

		#print "%s is running..." % process
	#else:

		print time.strftime("%Y-%d-%m %H:%M:%S")
		print "%s - ALERTA: %s is not running..." % (host,process)

		if cfg.send_email_alerta=="1":

			mail.send_msg_alert(cfg,"%s - ALERTA: %s não está rodando." % (host,process),"Olá.\n\nO serviço %s não está rodando. Corrija a situação o quanto antes.\n\nServiço WebSMS" % process)

		print "Trying command %s %s %s" % (startCmd,process.split(".")[0],startParm)

		#subprocess.call("%s %s %s" % (startCmd,process.split(".")[0],startParm),shell=True)
		subprocess.call("PATH='/bin:/sbin:/usr/bin:/usr/sbin:/opt/usr/bin:/opt/usr/sbin:/usr/local/bin:/usr/local/sbin' && %s %s %s" % (startCmd,process.split(".")[0],startParm),shell=True)
		time.sleep(5)

		if not verifySMSStatus(process):

			print time.strftime("%Y-%d-%m %H:%M:%S")
			print "%s - ALERTA: %s is not starting..." % (host,process)

			if cfg.send_email_alerta=="1":

				mail.send_msg_alert(cfg,"ALERTA: %s não pôde ser iniciado." % process,"Olá.\n\nO serviço %s não pôde ser iniciado e demanda de uma intervenção manual. Corrija a situação o quanto antes.\n\nServiço WebSMS" % process)
		else:
			print "%s - INFO: %s started..." % (host,process)

			if cfg.send_email_alerta=="1":

				mail.send_msg_alert(cfg,"%s - INFO: %s foi iniciado com sucesso." % (host,process),"Olá.\n\nO serviço %s foi iniciado com sucesso.\n\nServiço WebSMS" % process)
	
