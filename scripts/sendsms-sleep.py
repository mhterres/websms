#!/usr/bin/python
# -*- coding: utf-8 -*-

# sendsms.py
# Envio de SMS
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-05-11
#

import os
import sys
import time
import datetime
from random import randint
from unidecode import unidecode
from unicodedata import normalize

sys.path.insert(1,'/usr/lib/python2.7/dist-packages/sms')

from defs import validTime
from dbpgsql import DBPgsql
from config import Config
from dongle import Dongle

def verifyProcStatus(processname,minproc):

	tmp = os.popen("ps -Af").read()
	proccount = tmp.count(processname)

	if proccount > minproc:

		return True

	return False

pidfile='/var/tmp/sendsms.pid'

try:

	opt1=sys.argv[1] 
except:

	opt1=""

try:

	opt2=sys.argv[2] 
except:

	opt2=""

if opt1=="-d" or opt2=="-d":

	dryrun=True
else:

	dryrun=False

if opt1=="-c" or opt2=="-c":

	cron=True
	minproc=2
else:

	cron=False
	minproc=1

if verifyProcStatus("sendsms.py",minproc):

  print "sendsms.py is already running. Exiting"
  sys.exit(1)

if os.path.exists(pidfile):

	print time.strftime("%Y-%m-%d %H:%M:%S")
	print "sendsms.py exits with an error. Check logfile /tmp/sendsms.log"

if cron:
	
	print "Runnning cronjob."

if dryrun:

	print "Running in dry-run mode."
else:

	if not verifyProcStatus("smsstatus.py",0):

		print 'smsstatus.py is not running. Exiting.'
		sys.exit(1)

	print "Sending SMS."

pid = open(pidfile, "wb")
pid.write("%s\n" % str(os.getpid()))
pid.close()

cfg=Config()
DB=DBPgsql(cfg)
dongleSMS=Dongle()

# DEBUG
log = open('/tmp/sendsms.log', 'a')
sys.stderr = log

if not validTime(cfg,time.strftime("%Y-%m-%d %H:%M:%S")):

	os.remove(pidfile)
	sys.exit(1)

dongles=DB.statusDongles()

x=0
first=0
menor=999999

for dongle in dongles:

	if dongle['total']<menor:

		first=x
		menor=dongle['total']

	x = x + 1	

x=0

for dongle in dongles:

	if x == first:

		print "*ID: %i - Device: %s - Enviados: %s" % (dongle['id'],dongle['dongle'],dongle['total'])
	else:
		print "ID: %i - Device: %s - Enviados: %s" % (dongle['id'],dongle['dongle'],dongle['total'])

	x = x + 1	

sms=DB.smsNotSent()

devices=DB.getDonglesToSMS()

smsis=0
smsil=0

for item in sms:

	if item['tentativas']==0:

		newSMS=True
		nD=DB.nextDongle(devices)
	else:

		newSMS=False
		nD=DB.getDongle(item['dongle'].strip())

	if len(nD)==0:

		print "Todos dongles já enviaram a quantidade máxima de mensagens diárias"
		os.remove(pidfile)
		sys.exit(0)

	dID=nD['id']
	dongle=nD['dongle']
	sent=nD['sent']

	if item['dataini']==None or item['dataini']=="":

		processa=True

	else:

		agora=time.mktime(datetime.datetime.strptime(time.strftime("%Y-%m-%d %H:%M:%S"), "%Y-%m-%d %H:%M:%S").timetuple())

		dtini=time.mktime(item['dataini'].timetuple())

		if agora < dtini:

			processa=False
			print "Data Inicial msg %i ainda não alcançada." % item['id']
		else:

			processa=True

	if processa:

		if item['datafim'] != None and item['datafim'] != "":

			dtfim=time.mktime(item['datafim'].timetuple())

			if agora > dtfim:

				processa=False
				DB.lockSMS("V",item['id'])
				print "Data final msg %i já ultrapassada." % item['id']
			else:

				processa=True

	if processa:

		if item['tentativas']>=int(cfg.tentativas_envio):

			processa=False
			DB.lockSMS("C",item['id'])
			print "Mensagem %i já efetuou todas tentativas de envio." % item['id']

	if processa:

		if DB.optoutNumber(item['numero']):

			print "Destino %s inválido - Opt-Out" % item['numero']
			DB.lockSMS("O",item['id'])
			processa = False

	if processa:

		print "Mensagem %s para %s enviada pelo dongle %s" % (item['mensagem'],item['numero'],dongle)

		if not dryrun:

			randtime=randint(3,10)
			time.sleep(randtime)
	
			if smsis==50:

				randtime=randint(60,120)
				time.sleep(randtime)
				smsis=0

			if smsil==500:

				smsis=0
				smsil=0
				randtimepausa=randint(300,600)
				time.sleep(randtimepausa)

			smsis=smsis+1
			smsil=smsil+1
	
			idSMS=dongleSMS.ami.sendSMS(dongle,item['numero'],item['mensagem'])

			if idSMS != "":

				if item['tentativas']==0:

					DB.updateDongleData(time.strftime("%Y-%m-%d")+" 00:00:00",dID)

				DB.updateSMS(item['id'],dongle,idSMS,int(item['tentativas']+1),"F")
					
			else:

				print "Erro no envio do SMS id %s" % item['id']

		if newSMS:

			i=0

			for device in devices:

				if device['dongle']==dongle:

					devices[i]['sent']=sent
					break

				i = i +1


os.remove(pidfile)
sys.exit(0)
