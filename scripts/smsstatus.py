#!/usr/bin/python
# -*- coding: utf-8 -*-

# Pega Status do SMS via AMI
#
# Marcelo H. Terres <mhterres@gmail.com>
# 2015-05-21
#

import sys
import asterisk.manager
import datetime
import logging


sys.path.insert(1,'/usr/lib/python2.7/dist-packages/sms')

from ami import AMI
from config import Config
from dbpgsql import DBPgsql

cfg=Config()
DB=DBPgsql(cfg)

if cfg.debug=="1":

  logging.basicConfig(filename='/var/log/smsstatus.log',level=logging.DEBUG)
  logging.debug('Starting logging')

def handle_smsstatus(event, manager):
    print "Received SMS Status"
    headers=event.headers

    device=headers['Device']
    status=headers['Status']
    idSMS=headers['ID']
    #{'Privilege': 'call,all', 'Device': 'ch03', 'Status': 'Sent', 'Event': 'DongleSMSStatus', 'ID': '0x21ba368'}
    print "Device: %s" % device
    print "Status: %s" % status
    print "ID: %s" % idSMS
    print

    if cfg.debug=="1":
      
      logging.debug(datetime.datetime.today())
      logging.debug("Device: %s" % device)
      logging.debug("Status: %s" % status)
      logging.debug("ID: %s" % idSMS)
      logging.debug("")

    if status=="Sent":

        DB.updateSMSStatus(idSMS,device,"ok")
    else:

        DB.updateSMSStatus(idSMS,device,"err")

def handle_event(event, manager):
    print "Recieved event: %s" % event.name
   
manager = asterisk.manager.Manager()
try:
    # connect to the manager
    try:
        manager.connect(cfg.ami_host) 
        manager.login(cfg.ami_user,cfg.ami_pwd)
        # register some callbacks
        manager.register_event('DongleSMSStatus', handle_smsstatus) 
        #manager.register_event('*', handle_event)           # catch all
          
        # get a status report
        response = manager.status()
    except asterisk.manager.ManagerSocketException as err:
        errno, reason = err
        print ("Error connecting to the manager: %s" % reason)
        sys.exit(1)
    except asterisk.manager.ManagerAuthException as reason:
        print ("Error logging in to the manager: %s" % reason)
        sys.exit(1)
    except asterisk.manager.ManagerException as reason:
        print ("Error: %s" % reason)
        sys.exit(1)
       
finally:
    i=0
    while True:

        i=i+1

        if i>100000:

            i=0

